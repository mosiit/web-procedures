USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ACTIVE_MEMBERSHIPS]
	@MembershipActiveDate DATETIME,
	@category_str VARCHAR(MAX)  
AS

BEGIN 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	-- DSJ 9/12/2016
	-- GET ACTIVE MEMBERSHIPS AS OF A DATE
	-- NEED TO LOOK IN BOTH VIEWS - LV_CURRENT_MEMBERSHIP_INFO AND LV_PAST_MEMBERSHIP_INFO

DECLARE @MembCategory TABLE
(
	category VARCHAR(30) NOT NULL
)

-- IF @category_str is NULL, search ALL TR_MEMB_LEVEL_CATEGORY
IF ISNULL(@category_str, '') <> ''
BEGIN
	INSERT INTO @MembCategory
	SELECT REPLACE(CONVERT(VARCHAR(30),Element), CHAR(34), '') FROM dbo.FT_SPLIT_LIST (@category_str,',')
END
ELSE 
BEGIN
	INSERT @MembCategory
	SELECT description FROM TR_MEMB_LEVEL_CATEGORY

END 

	DECLARE @ActiveMembershipsTbl TABLE
	(
		 customer_no INT,
		 customer_display_name VARCHAR(200),
		 memb_level_category_description VARCHAR(30), 
		 memb_level_description VARCHAR(30), 
		 initiation_date DATETIME, 
		 expiration_date DATETIME, 
		 current_status_desc VARCHAR(30) 
	)

	INSERT INTO @ActiveMembershipsTbl
	(
		customer_no,
		customer_display_name,
		memb_level_category_description,
		memb_level_description,
		initiation_date,
		expiration_date,
		current_status_desc
	)
	SELECT customer_no, customer_display_name,memb_level_category_description, memb_level_description, initiation_date, expiration_date, 'Active' AS current_status_desc
	FROM dbo.LV_CURRENT_MEMBERSHIP_INFO info
		INNER JOIN @MembCategory cat
		ON info.memb_level_category_description = cat.category
	WHERE @MembershipActiveDate BETWEEN inception_date AND expiration_date
	UNION ALL 
	-- Don't want to double count any non-current memberships if they are already ACTIVE.
	SELECT DISTINCT customer_no, customer_display_name,memb_level_category_description, memb_level_description, initiation_date, expiration_date, current_status_desc
	FROM dbo.LV_PAST_MEMBERSHIP_INFO info
		INNER JOIN @MembCategory cat 
		ON info.memb_level_category_description = cat.category
	WHERE @MembershipActiveDate BETWEEN inception_date AND expiration_date
	AND customer_no NOT IN (SELECT customer_no FROM dbo.LV_CURRENT_MEMBERSHIP_INFO WHERE @MembershipActiveDate BETWEEN inception_date AND expiration_date) 

	-- Get counts of all Active MOS Members by category and level
	SELECT memb_level_category_description, memb_level_description, COUNT(*) AS cnt
	FROM @ActiveMembershipsTbl
	GROUP BY memb_level_category_description, memb_level_description
	ORDER BY memb_level_category_description, memb_level_description

END 