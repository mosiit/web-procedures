USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_CONSTITUENT_SEARCH_FROM_MEMBERSHIP_NO_EX]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LP_CONSTITUENT_SEARCH_FROM_MEMBERSHIP_NO_EX]
GO

CREATE PROCEDURE [dbo].[LP_CONSTITUENT_SEARCH_FROM_MEMBERSHIP_NO_EX]
    @last_name varchar(120),
    @cust_memb_no varchar(120)
AS

    Declare	@customer_no int
    SELECT @customer_no = @cust_memb_no

    SELECT * FROM T_CUSTOMER
    WHERE
        lname = @last_name AND
        (
            customer_no = @customer_no OR
            customer_no IN (SELECT individual_customer_no FROM T_AFFILIATION WHERE individual_customer_no = @customer_no OR group_customer_no = @customer_no)
        )
GO
GRANT EXECUTE ON [dbo].[LP_CONSTITUENT_SEARCH_FROM_MEMBERSHIP_NO_EX] TO ImpUsers;
GO
GRANT ALTER ON [dbo].[LP_CONSTITUENT_SEARCH_FROM_MEMBERSHIP_NO_EX] TO [mos.org\lthornton];
GO
