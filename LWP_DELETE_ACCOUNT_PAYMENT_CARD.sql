USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_DELETE_ACCOUNT_PAYMENT_CARD]    Script Date: 02/01/2016 09:11:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LWP_DELETE_ACCOUNT_PAYMENT_CARD]    Script Date: 05/30/2014 07:54:15 ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[LWP_DELETE_ACCOUNT_PAYMENT_CARD]')
                    AND type IN (N'P', N'PC') ) 
   DROP PROCEDURE [dbo].[LWP_DELETE_ACCOUNT_PAYMENT_CARD]
GO


CREATE PROCEDURE [dbo].[LWP_DELETE_ACCOUNT_PAYMENT_CARD]
	@sessionkey VARCHAR(64),
	@act_id INT
	with execute as 'dbo'
AS
BEGIN
	
	DECLARE @customer_no int, @id_key int, @errmsg varchar(255)

	-- validate sessionkey parameter
	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End
	Else
		Select @customer_no = customer_no from t_web_session_session where sessionkey = @sessionkey


	UPDATE T_PAYMENT SET act_id = 0 WHERE act_id = @act_id AND customer_no = @customer_no;
	UPDATE T_ACCOUNT_DATA SET inactive = 'Y' WHERE id = @act_id AND customer_no = @customer_no;
	-- DELETE FROM T_ACCOUNT_DATA WHERE id = @act_id;
	IF @@ROWCOUNT > 0
	BEGIN 
		SELECT 'Y' AS success
	END 
	ELSE
		SELECT 'N' AS success;

END

GO

GRANT EXECUTE ON [dbo].[LWP_DELETE_ACCOUNT_PAYMENT_CARD] TO impusers
GO