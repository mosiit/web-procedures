USE [impresario];
GO
/****** Object:  StoredProcedure [dbo].[LWP_REMOVE_TPP_TOKEN]    Script Date: 02/01/2016 09:11:18 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

/****** Object:  StoredProcedure [dbo].[LWP_REMOVE_TPP_TOKEN]    Script Date: 05/30/2014 07:54:15 ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[LWP_REMOVE_TPP_TOKEN]')
                    AND type IN (N'P', N'PC') )
    DROP PROCEDURE [dbo].[LWP_REMOVE_TPP_TOKEN];
GO

CREATE PROCEDURE [dbo].[LWP_REMOVE_TPP_TOKEN]
    @sessionkey VARCHAR(255),
    @TPPToken VARCHAR(255)
    WITH EXECUTE AS 'dbo'
AS
BEGIN

    DECLARE @errmsg VARCHAR(255);

    IF @sessionkey IS NULL
        OR NOT EXISTS ( SELECT  *
                        FROM    t_web_session_Session
                        WHERE   SessionKey = @sessionkey )
        BEGIN
            SELECT  @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey;
            RAISERROR(@errmsg, 11, 2) WITH SETERROR;
            RETURN -101;
        END;
    ELSE
        DELETE  FROM [dbo].[LTR_WEB_TPP_TOKEN]
        WHERE   token = @TPPToken;

END;

GO

GRANT EXECUTE ON [dbo].[LWP_REMOVE_TPP_TOKEN] TO ImpUsers;
GO
