USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD]    Script Date: 02/01/2016 09:11:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD]    Script Date: 05/30/2014 07:54:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD]
GO

CREATE PROCEDURE [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD]
	@sessionkey VARCHAR(64),
	@token VARCHAR(64),
	@act_type INT,
	@expiry_month VARCHAR(2),
	@expiry_year  VARCHAR(4),
	@act_no_four  VARCHAR(4),
	@act_no_six  VARCHAR(6),
	@act_name VARCHAR(128),
	@inactive VARCHAR(1)
	with execute as 'dbo'
AS
BEGIN
	DECLARE @customer_no int, @id_key int, @errmsg varchar(255)

	-- validate sessionkey parameter
	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End
	Else
		Select @customer_no = customer_no from t_web_session_session where sessionkey = @sessionkey



	DECLARE @cd_no int
	execute @cd_no = impresario..ap_get_nextid_function   @type = 'CD'

	OPEN SYMMETRIC KEY TessituraSecureKey
	DECRYPTION BY CERTIFICATE TessCert
    
    INSERT INTO dbo.T_ACCOUNT_DATA
    (
		id,
		customer_no,
		act_no_encrypt,
		act_type,
		act_name,
		card_expiry_dt,
		token,
		inactive,
		payment_method_group_id
    )
	VALUES
	(
		@cd_no,
		@customer_no,
		@act_no_six + 'xxxxxx' + @act_no_four,
		@act_type,
		@act_name,
		@expiry_year + '-' + @expiry_month + '-01',
		@token,
		@inactive,
		1
	);
	CLOSE SYMMETRIC KEY TessituraSecureKey
	SELECT @cd_no
END
GO

GRANT EXECUTE ON [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD] TO impusers
GO
