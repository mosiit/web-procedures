USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_FETCH_PAYMENT_ACCOUNTS_FOR_UPDATE]    Script Date: 9/12/2017 11:50:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LWP_FETCH_PAYMENT_ACCOUNTS_FOR_UPDATE]
    @renewal_period INT
  WITH execute as 'dbo'
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


DECLARE @activeMemberships TABLE(
  customer_no INT NOT NULL,
  token VARCHAR(100) NULL,
  id INT NOT NULL
);

DECLARE @pendingMemberships TABLE(
  customer_no INT NOT NULL,
  token VARCHAR(100) NULL,
  id INT NOT NULL
);

/**
 * Set of active memberships
 */
INSERT INTO @activeMemberships(customer_no, token, id)
  SELECT m.customer_no, a.token, a.id
    FROM [impresario].[dbo].[TX_CUST_MEMBERSHIP] AS m
    JOIN [impresario].[dbo].[TX_CUST_KEYWORD] AS k ON m.customer_no = k.customer_no
    JOIN [impresario].[dbo].[T_ACCOUNT_DATA] AS a ON m.customer_no = a.customer_no
    WHERE m.memb_org_no = 4 AND
    m.current_status = 2 AND
    DATEDIFF(day, GETDATE(), CAST(m.expr_dt AS DATE)) = @renewal_period AND
    k.keyword_no = 417 AND
    k.key_value = convert(varchar(10), a.id) AND
    a.inactive = 'N'

/**
 * Set of renewed memberships
 */
INSERT INTO @pendingMemberships(customer_no, token, id)
SELECT a.customer_no, a.token, a.id FROM @activeMemberships AS a
JOIN [impresario].[dbo].[TX_CUST_MEMBERSHIP] AS m ON a.customer_no = m.customer_no
WHERE m.current_status = 3

/**
 * Set of memberships that are due for renewal excluding ones that are pending renewal 
 */
SELECT * FROM @activeMemberships
EXCEPT
SELECT * FROM @pendingMemberships

END

GO
GRANT EXECUTE ON [dbo].[LWP_FETCH_PAYMENT_ACCOUNTS_FOR_UPDATE] TO ImpUsers
GO
