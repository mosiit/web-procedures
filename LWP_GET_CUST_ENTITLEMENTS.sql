USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_GET_CUST_ENTITLEMENTS]    Script Date: 9/22/2017 10:17:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LWP_GET_CUST_ENTITLEMENTS]

@sessionkey varchar(64) = NULL,
@customer_no INT = NULL,
@calendar_date datetime = NULL,
@reset_type char(1) = 'A', /* D=Daily; M=Membership; A (or null)=All */
@debug char(1) = 'N'

AS

SET NOCOUNT ON

/************************************************************************
3/25/2015 Michael Reisman
Retrieves list of current entitlements for customer, for web use

--customer given
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS @customer_no = 70039, @reset_type = 'M'
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS @sessionkey = '4JFUV0NVARID3Q3L110UOYHU49KJ8CRO0323S9DN8O56214O8E616XQ5P85Y6738', @reset_type = 'M'
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS @customer_no = 10
select * FROM T_WEB_ORDER WHERE sessionkey = '4JFUV0NVARID3Q3L110UOYHU49KJ8CRO0323S9DN8O56214O8E616XQ5P85Y6738'
select * FROM T_WEB_CONTRIBUTION WHERE order_no = 164

Modified
4/30/2015 MIR Includes Cart memberships.  Assumes membership will start TODAY.
5/1/2015 MIR Fix to use cust_memb_no on join from TX_CUST_MEMBERSHIP to LTX_CUST_ENTITLEMENT
5/9/2015 MIR picking a @cart_memb_level_no at random if there are more than one.
5/12/2015 MIR when finding cart memberships, ignore where fund # is blank.
5/13/2015 MIR using max fund if there are two with the same amt.
6/22/2015 MIR inactive status should not be granted entitlements - only 2
6/22/2016 MIR Getting customer from T_WEB_SESSION_SESSION, not T_WEB_ORDER
6/28/2016 MIR using member_level_no from T_WEB_CONTRIBUTION if provided
************************************************************************/

Declare @errmsg varchar(200)
DECLARE @cart_order_no int
DECLARE @cart_memb_level_no int
DECLARE @cart_cont_amt money 
DECLARE @cart_fund_no int

If @sessionkey is NOT NULL AND not exists (select * from [dbo].t_web_session_session where sessionkey = @sessionkey)
  Begin
	select @errmsg = 'Invalid Session ID'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	return -101
  End

IF @sessionkey IS NOT NULL
  BEGIN
	/* get customer no */
	SELECT @customer_no = customer_no FROM T_WEB_SESSION_SESSION where sessionkey = @sessionkey
  
	/* get the order no */
	SELECT @cart_order_no = order_no FROM T_WEB_ORDER WHERE sessionkey = @sessionkey
  
	SELECT TOP 1 @cart_memb_level_no = memb_level_no
	FROM dbo.T_WEB_CONTRIBUTION 
	WHERE order_no = @cart_order_no
	ORDER BY contribution_amt DESC

	IF ISNULL(@cart_memb_level_no,0) = 0
	  BEGIN
		/* Get the in-cart membership from data in T_WEB_CONTRIBUTION. There can be more than one contribution at one time in the cart, take highest amt */
		SET @cart_cont_amt = (SELECT MAX(contribution_amt) FROM T_WEB_CONTRIBUTION wc WHERE order_no = @cart_order_no AND fund_no IS NOT NULL) -- 5/12/2015
		SET @cart_fund_no = (SELECT MAX(fund_no) FROM dbo.T_WEB_CONTRIBUTION WHERE order_no = @cart_order_no AND contribution_amt = @cart_cont_amt AND fund_no is NOT NULL) --5/12/2015

		IF @debug = 'Y' SELECT cart_cont_amt = @cart_cont_amt, cart_fund_no = @cart_fund_no
  
		SELECT @cart_memb_level_no = c.memb_level_no /* picking one at random */
		FROM TX_CAMP_FUND a
			JOIN T_CAMPAIGN b on a.campaign_no = b.campaign_no
			JOIN T_MEMB_LEVEL c on c.memb_org_no = b.memb_org_no 
		where fund_no = @cart_fund_no 
		AND CURRENT_TIMESTAMP BETWEEN a.start_dt AND a.end_dt 
		AND @cart_cont_amt BETWEEN c.start_amt and c.end_amt 
		and b.memb_org_no is NOT NULL
	  END
  END

IF @debug = 'Y' SELECT cart_memb_level_no = @cart_memb_level_no

SET @calendar_date = ISNULL(@calendar_date, 
					(SELECT CAST(CONVERT(varchar(10),CURRENT_TIMESTAMP,101) as datetime)))

SELECT
	e.entitlement_no,
	e.memb_level_no,
	mem_org_desc = mo.description,
	mem_level_desc = ml.description,
	entitlement_keyword_desc = kw.description,
	reset_type_desc = gd.description,
	price_type_group = pg.id,
	price_type_group_desc = pg.description,
	num_start_ent = COALESCE(ce.num_ent,e.num_ent),
	num_remain_ent = COALESCE(ce.num_ent,e.num_ent) - ISNULL(coe.cust_num_used,0),
	e.custom_1,
	e.custom_2,
	e.custom_3,
	e.custom_4,
	e.custom_5,
	e.custom_6,
	e.custom_7,
	e.custom_8,
	e.custom_9,
	e.custom_10,
	cm.current_status
FROM dbo.LTR_ENTITLEMENT e
	JOIN dbo.TR_TKW kw ON e.ent_tkw_id = kw.id
	JOIN dbo.T_MEMB_LEVEL ml ON e.memb_level_no = ml.memb_level_no
	JOIN dbo.T_MEMB_ORG mo ON mo.memb_org_no = ml.memb_org_no
	--JOIN dbo.TX_CUST_MEMBERSHIP cm ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level AND cm.cur_record = 'Y')
	JOIN (
		SELECT
			customer_no,
			cust_memb_no,
			memb_org_no,
			memb_level,
			init_dt,
			expr_dt,
			current_status
		FROM TX_CUST_MEMBERSHIP 
		WHERE customer_no = @customer_no
		AND cur_record = 'Y'
		
		UNION 
	
		SELECT 
			customer_no = @customer_no,
			cust_memb_no = 0,
			memb_org_no,
			memb_level,
			init_dt = CAST(CONVERT(varchar(10),CURRENT_TIMESTAMP,101) as datetime),
			expr_dt = DATEADD(SECOND,-1,(DATEADD(MONTH,ml2.expiry,CURRENT_TIMESTAMP))),
			current_status = 2
		FROM (SELECT memb_level_no = @cart_memb_level_no) cart_mem
			JOIN T_MEMB_LEVEL ml2 ON ml2.memb_level_no = cart_mem.memb_level_no) cm
	ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level)
	JOIN dbo.TR_GOOESOFT_DROPDOWN gd ON (gd.code = 1002 AND gd.short_desc = e.reset_type)
	JOIN dbo.TR_PRICE_TYPE_GROUP pg ON pg.id = e.ent_price_type_group
	LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
		ON (e.entitlement_no = ce.entitlement_no 
		AND ce.customer_no = cm.customer_no
		AND ce.cust_memb_no = cm.cust_memb_no
		AND (e.reset_type = 'M'
			OR 
			(e.reset_type = 'D' AND CONVERT(VARCHAR(10),CURRENT_TIMESTAMP,101) = CONVERT(VARCHAR(10),ce.entitlement_date,101))
			))
	LEFT JOIN (
		SELECT
			customer_no,
			cust_memb_no,
			entitlement_no, 
			entitlement_date,
			cust_num_used = ISNULL(SUM(num_used),0)
		FROM dbo.LTX_CUST_ORDER_ENTITLEMENT
		GROUP BY customer_no, cust_memb_no, entitlement_no, entitlement_date) coe
	ON (
		coe.customer_no = cm.customer_no 
		AND coe.cust_memb_no = cm.cust_memb_no
		AND coe.entitlement_no = e.entitlement_no
		AND (
			(reset_type = 'D' AND coe.entitlement_date = @calendar_date)
			OR reset_type = 'M'
			)
		)
WHERE cm.customer_no = @customer_no
AND @calendar_date BETWEEN cm.init_dt and cm.expr_dt
AND cm.current_status = 2
AND (
	ISNULL(@reset_type,'A') = 'A'
	OR e.reset_type = @reset_type
	)
ORDER BY kw.description
