USE [impresario]

GO
/****   (2015-09-17)    ****/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_DONATION_CONFIG_LOOKUP]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_DONATION_CONFIG_LOOKUP]
GO

CREATE PROCEDURE [dbo].[LP_DONATION_CONFIG_LOOKUP]
    @session_key varchar(64) = null,
    @rank int = 0
AS BEGIN

    SET NOCOUNT ON

    DECLARE @customer_no int = null, @errmsg varchar(255)

    IF @session_key is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @session_key)
        BEGIN
            SELECT errmsg = 'Invalid Customer Session ID'
            RAISERROR(@errmsg, 11, 2) WITH SETERROR
            return -101
        END
    ELSE
        /** The @rank may not be correct if it's on an attribute **/
        SELECT @customer_no = customer_no
        FROM t_web_session_session
        WHERE sessionkey = @session_key


        SELECT TOP 1 * FROM [dbo].[LTR_WEB_DONATION_CONFIG]
        WHERE
            (constituent_no = @customer_no AND rank = @rank)
            OR
            (constituent_no = 0 AND rank = @rank)
        ORDER BY constituent_no DESC;
END
GO

GRANT EXECUTE ON [dbo].[LP_DONATION_CONFIG_LOOKUP] TO impusers
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_DONATION_CONFIG_LOOKUP') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Donation Config Lookup','LP_DONATION_CONFIG_LOOKUP')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_DONATION_CONFIG_LOOKUP'




