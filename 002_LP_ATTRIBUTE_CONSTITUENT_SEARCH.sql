USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ATTRIBUTE_CONSTITUENT_SEARCH]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LWP_ATTRIBUTE_CONSTITUENT_SEARCH]
GO

CREATE PROCEDURE [dbo].[LWP_ATTRIBUTE_CONSTITUENT_SEARCH]
        @keyword_id int,
        @first_name varchar(120),
        @last_name varchar(120),
        @attribute_value varchar(120)
AS  BEGIN
   
    Declare	@keyword_no int

    SELECT @keyword_no = keyword_no FROM TX_KEYWORD_CUST_TYPE
    WHERE id = @keyword_id

    SELECT * FROM TX_CUST_KEYWORD AS lookup
    INNER JOIN T_CUSTOMER AS c
    ON c.fname = @first_name AND c.lname = @last_name AND lookup.customer_no = c.customer_no
    WHERE keyword_no = @keyword_no AND key_value = @attribute_value

END
GO

GRANT EXECUTE ON [dbo].[LWP_ATTRIBUTE_CONSTITUENT_SEARCH] TO Impusers
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LWP_ATTRIBUTE_CONSTITUENT_SEARCH') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Attribute Constituent Search','LWP_ATTRIBUTE_CONSTITUENT_SEARCH')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LWP_ATTRIBUTE_CONSTITUENT_SEARCH'
