USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LWP_ADD_ATTRIBUTE_WITHOUT_SESSION]
  @customer_no INT,
  @keyword_no INT,
  @key_value VARCHAR(30)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

  INSERT INTO [dbo].[TX_CUST_KEYWORD]
  (
    keyword_no,
    customer_no,
    key_value
  )
  VALUES
  (
    @keyword_no,
    @customer_no,
    @key_value
  )
END

GO
GRANT EXECUTE ON [dbo].[LWP_ADD_ATTRIBUTE_WITHOUT_SESSION] TO ImpUsers