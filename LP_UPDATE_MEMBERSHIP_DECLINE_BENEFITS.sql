USE [impresario]

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_UPDATE_MEMBERSHIP_DECLINE_BENEFITS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LP_UPDATE_MEMBERSHIP_DECLINE_BENEFITS]
GO

USE [impresario]

GO

CREATE PROCEDURE [dbo].[LP_UPDATE_MEMBERSHIP_DECLINE_BENEFITS]
    @customer_no int
AS
    UPDATE TX_CUST_MEMBERSHIP
     SET declined_ind = 'Y'
     WHERE cust_memb_no=(SELECT TOP 1 cust_memb_no FROM TX_CUST_MEMBERSHIP WHERE customer_no = @customer_no ORDER BY create_dt DESC)

    UPDATE LTX_CUST_ENTITLEMENT
     SET num_ent = 0
     WHERE customer_no=@customer_no AND cust_memb_no=(SELECT TOP 1 cust_memb_no FROM TX_CUST_MEMBERSHIP WHERE customer_no = @customer_no ORDER BY create_dt DESC)

GO
 
GRANT EXECUTE ON [dbo].[LP_UPDATE_MEMBERSHIP_DECLINE_BENEFITS] TO ImpUsers
GO
