USE [impresario]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD]')
                    AND type IN (N'P', N'PC') ) 
   DROP PROCEDURE [dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD]
GO

CREATE PROCEDURE [dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD]
       @sessionkey VARCHAR(64),
       @act_id INT,
       @payment_method_group_id INT
       WITH EXECUTE AS 'dbo'
AS 
       BEGIN
             DECLARE @customer_no INT,
                     @id_key INT,
                     @errmsg VARCHAR(255)

             IF @sessionkey IS NULL
                OR NOT EXISTS ( SELECT  *
                                FROM    t_web_session_session
                                WHERE   sessionkey = @sessionkey ) 
                BEGIN
                      SELECT    @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
                      RAISERROR(@errmsg, 11, 2) WITH SETERROR
                      RETURN -101
                END
             ELSE 
                SELECT  @customer_no = customer_no
                FROM    t_web_session_session
                WHERE   sessionkey = @sessionkey

             OPEN SYMMETRIC KEY TessituraSecureKey
			 DECRYPTION BY CERTIFICATE TessCert

             UPDATE dbo.T_ACCOUNT_DATA
             SET    payment_method_group_id = 1
             WHERE  customer_no = @customer_no
                    AND payment_method_group_id = 3
                    AND id <> @act_id
             UPDATE dbo.T_ACCOUNT_DATA
             SET    payment_method_group_id = 3
             WHERE  customer_no = @customer_no
                    AND id = @act_id
       END
GO

GRANT ALTER ON [dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD] TO [MOS.ORG\lthornton]
GO
GRANT CONTROL ON [dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD] TO [MOS.ORG\lthornton]
GO
GRANT EXECUTE ON [dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD] TO [MOS.ORG\lthornton]
GO

GRANT EXECUTE ON [dbo].[LWP_UPDATE_ACCOUNT_PAYMENT_CARD] TO [ImpUsers]
GO