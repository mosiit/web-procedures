USE [impresario]
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_TEACHER_ORDER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_TEACHER_ORDER]
GO

CREATE PROCEDURE [dbo].[LWP_ADD_TEACHER_ORDER]
    @teacher_customer_no INT,
    @school_customer_no INT,
    @school_order_no INT
AS
BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    INSERT INTO dbo.LTR_TEACHER_ORDERS
    (
        teacher_customer_no,
        school_customer_no,
        school_order_no
    ) VALUES (
        @teacher_customer_no,
        @school_customer_no,
        @school_order_no
    );
END

GO
GRANT EXECUTE ON [dbo].[LWP_ADD_TEACHER_ORDER] TO ImpUsers
GO
