USE [impresario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LWP_MEMBERSHIP_GIFT_LOOKUP_EX]
    @sessionkey VARCHAR(255),
    @membership_no INT,
    @email VARCHAR(255),
    @zip VARCHAR(255),
    @lastName VARCHAR(255)
AS BEGIN

  DECLARE @errmsg VARCHAR(255)
  DECLARE @groupCustNo INT

   SELECT @groupCustNo = group_customer_no FROM T_AFFILIATION WHERE individual_customer_no = @membership_no OR group_customer_no = @membership_no

    IF @sessionkey is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @sessionkey) BEGIN

      SELECT @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
      RAISERROR(@errmsg, 11, 2) WITH SETERROR
      return -101

    END ELSE BEGIN

	SELECT customer.customer_no
      INTO #membershipsViaNumber
      FROM T_CUSTOMER customer
      JOIN T_EADDRESS eaddress
      ON eaddress.primary_ind = 'Y' AND customer.customer_no = eaddress.customer_no
      WHERE
      (
        eaddress.address = @email OR
        customer.customer_no IN (SELECT customer_no FROM T_EADDRESS WHERE customer_no = customer.customer_no AND address = @email)
      ) AND
      (
        customer.customer_no = @membership_no OR
        customer.customer_no IN (SELECT individual_customer_no FROM T_AFFILIATION WHERE individual_customer_no = @membership_no OR group_customer_no = @membership_no)
      )

      IF (SELECT COUNT(*) FROM  #membershipsViaNumber) > 0 BEGIN

        SELECT * FROM #membershipsViaNumber
        return

      END

      SELECT customer.customer_no
	    INTO #membershipsViaAddress
      FROM T_CUSTOMER customer
      JOIN T_ADDRESS address
      ON address.primary_ind = 'Y' AND customer.customer_no = address.customer_no OR address.customer_no = (SELECT @groupCustNo)
      WHERE customer.lname = @lastName AND (
        customer.customer_no = @membership_no OR
        customer.customer_no IN (SELECT individual_customer_no FROM T_AFFILIATION WHERE individual_customer_no = @membership_no OR group_customer_no = @membership_no)
      ) 
      -- AND (
      --  address.postal_code LIKE @zip + '%' OR
      --  customer.customer_no IN (SELECT customer_no FROM T_ADDRESS WHERE (customer_no = customer.customer_no OR customer_no = (SELECT @groupCustNo)) AND postal_code LIKE @zip + '%')
      --)

      IF (SELECT COUNT(*) FROM  #membershipsViaAddress) > 0 BEGIN

        SELECT * FROM #membershipsViaAddress
        return

      END

      return

    END
END

GO
 
GRANT EXECUTE ON [dbo].[LWP_MEMBERSHIP_GIFT_LOOKUP_EX] TO ImpUsers
GO
