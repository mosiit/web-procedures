USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_CC_SERVER_LOG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_CC_SERVER_LOG]
GO

CREATE PROCEDURE [dbo].[LWP_ADD_CC_SERVER_LOG]
    @customerNo VARCHAR(100),
    @lastFour VARCHAR(4),
    @expiryDate VARCHAR(4),
    @merchantId VARCHAR(4),
    @approvalCode VARCHAR(100),
    @reference VARCHAR(100)
    with execute as 'dbo'
AS
BEGIN
    DECLARE @payment_no int, @payment_date DATETIME, @amount VARCHAR(10)

    SELECT TOP 1 
        @payment_no = payment_no,
        @payment_date = pmt_dt,
        @amount = REPLACE(pmt_amt, '.', '')
    FROM T_PAYMENT 
    WHERE customer_no = @customerNo 
    ORDER BY pmt_dt DESC

    DECLARE @unique_key int
    execute @unique_key = impresario..ap_get_nextid_function @type = 'SC'

    INSERT INTO dbo.T_CC_SERVER_LOG
    (
    	unique_key,
    	tran_date,
    	payment_no,
    	tran_type,
    	cc_number,
    	cc_expir_date,
    	amount,
    	merchant_id,
    	terminal_identifier,
    	order_number,
    	created_by,
    	response_code,
    	response_msg,
    	pb_response_message,
    	approval_code,
    	reference_number
    ) VALUES (
        @unique_key,
        @payment_date,
        @payment_no,
        'B',
        @lastFour,
        @expiryDate,
        @amount,
        @merchantId,
        'WebAPI',
        @payment_no,
        'WebAPI',
        '00',
        'Success',
        'Hosted Payment/Kiosk',
        @approvalCode,
        @reference
    );
END
GO

GRANT ALTER ON [dbo].[LWP_ADD_CC_SERVER_LOG] TO [MOS.ORG\lthornton]
GO
GRANT CONTROL ON [dbo].[LWP_ADD_CC_SERVER_LOG] TO [MOS.ORG\lthornton]
GO
GRANT EXECUTE ON [dbo].[LWP_ADD_CC_SERVER_LOG] TO [MOS.ORG\lthornton]
GO

GRANT EXECUTE ON [dbo].[LWP_ADD_CC_SERVER_LOG] TO ImpUsers
GO