USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_GET_CUST_FROM_CARD_TOKEN]    Script Date: 5/15/2016 2:50:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_GET_CUST_FROM_CARD_TOKEN]
        @SessionKey VARCHAR(255) = null,
	    @lastDigits VARCHAR(4) = null,
	    @expiryMonth VARCHAR(2) = null,
	    @expiryYear VARCHAR(2) = null
AS BEGIN

    SELECT DISTINCT customer_no
	INTO #accountData
	FROM [dbo].[T_ACCOUNT_DATA]
	WHERE act_no_four = @lastDigits
	  AND month(card_expiry_dt) = @expiryMonth
	  AND year(card_expiry_dt) = concat('20', @expiryYear)

	SELECT DISTINCT customer_no
	INTO #paymentData
	FROM [impresario].[dbo].[T_PAYMENT]
	WHERE account_no = @lastDigits
	  AND month(card_expiry_dt) = @expiryMonth
	  AND year(card_expiry_dt) = concat('20', @expiryYear)

	SELECT * FROM #paymentData UNION SELECT * FROM #accountData
END