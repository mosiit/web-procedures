USE [impresario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[LWP_GET_FIELD_TRIP_OPTION]')
                    AND type IN (N'P', N'PC') ) 
   DROP PROCEDURE [dbo].[LWP_GET_FIELD_TRIP_OPTION]
GO

CREATE PROCEDURE [dbo].[LWP_GET_FIELD_TRIP_OPTION] @keyword INT
       WITH EXECUTE AS 'dbo'
AS 
       BEGIN

             SELECT key_value
             FROM   [dbo].[T_KWCODED_VALUES]
             WHERE  keyword_no = @keyword

       END

GO
GRANT ALTER ON [dbo].[LWP_GET_FIELD_TRIP_OPTION] TO [MOS.ORG\lthornton]
GO
GRANT CONTROL ON [dbo].[LWP_GET_FIELD_TRIP_OPTION] TO [MOS.ORG\lthornton]
GO
GRANT EXECUTE ON [dbo].[LWP_GET_FIELD_TRIP_OPTION] TO [MOS.ORG\lthornton]
GO
GRANT EXECUTE ON [dbo].[LWP_GET_FIELD_TRIP_OPTION] TO [ImpUsers]
GO

