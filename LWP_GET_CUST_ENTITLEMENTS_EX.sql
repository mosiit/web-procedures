USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LWP_GET_CUST_ENTITLEMENTS]    Script Date: 1/5/2017 7:46:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LWP_GET_CUST_ENTITLEMENTS_EX]

@sessionkey VARCHAR(64) = NULL,
@customer_no INT = NULL,
@calendar_date DATETIME = NULL,
@reset_type CHAR(1) = 'A' /* D=Daily; M=Membership; A (or null)=All */

AS

SET NOCOUNT ON

/************************************************************************
3/25/2015 Michael Reisman
Retrieves list of current entitlements for customer, for web use

--customer given
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS @customer_no = 70017, @calendar_date = NULL, @reset_type = 'A'
--customer not given
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS @sessionkey  = '9BYH7XQHX8H1IEJ283XLXHDBM29WPDUQE029WW32Q0XVWWDBB823OYOU5OC80029', @calendar_date = NULL, @reset_type = 'A'

Modified
4/28/2015 Splinter code for MFA
4/30/2015 Includes Cart memberships and logic to determine if the new memberhsip will be pending or upgrade
5/9/2015 MIR picking a @cart_memb_level_no at random if there are more than one.
5/12/2015 MIR when finding cart memberships, ignore where fund # is blank.
5/13/2015 MIR using max fund if there are two with the same amt.
6/22/2015 MIR inactive status should not be granted entitlements - only (2,3)
6/26/2015 MIR includes one more column -- current status -- to handle pendings vs actives later.
************************************************************************/

DECLARE @errmsg varchar(200)
DECLARE @cart_order_no int
DECLARE @cart_memb_level_no int
DECLARE @cart_cont_amt money 
DECLARE @cart_fund_no int

If @sessionkey is NOT NULL AND not exists (select * from [dbo].t_web_session_session where sessionkey = @sessionkey)
  Begin
	select @errmsg = 'Invalid Session ID'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	RETURN -101
  END

IF @sessionkey IS NOT NULL
  BEGIN
	/* get the order and customer */
	SELECT
		@cart_order_no = order_no,
		@customer_no = customer_no
	FROM T_WEB_ORDER 
	WHERE sessionkey = @sessionkey
  
	/* Get the in-cart membership from data in T_WEB_CONTRIBUTION. There can be more than one contribution at one time in the cart, take highest amt */
	SET @cart_cont_amt = (SELECT MAX(contribution_amt) FROM T_WEB_CONTRIBUTION wc WHERE order_no = @cart_order_no AND fund_no IS NOT NULL) -- 5/12/2015
	SET @cart_fund_no = (SELECT MAX(fund_no) FROM dbo.T_WEB_CONTRIBUTION WHERE order_no = @cart_order_no AND contribution_amt = @cart_cont_amt AND fund_no is NOT NULL) --5/12/2015

	SELECT @cart_memb_level_no = c.memb_level_no /* picking one at random: fix 5/9/2015 */
	FROM TX_CAMP_FUND a
		JOIN T_CAMPAIGN b ON a.campaign_no = b.campaign_no
		JOIN T_MEMB_LEVEL c ON c.memb_org_no = b.memb_org_no 
	WHERE fund_no = @cart_fund_no 
	AND CURRENT_TIMESTAMP BETWEEN a.start_dt AND a.end_dt 
	AND @cart_cont_amt BETWEEN c.start_amt AND c.end_amt 
	AND b.memb_org_no IS NOT NULL
  END

/* Find existing current membership */
SELECT 
	cm.memb_org_no,
	init_dt,
	expr_dt,
	ml.renewal,
	ml.expiry,
	cm.current_status
INTO #active_membs
FROM TX_CUST_MEMBERSHIP cm
	JOIN T_MEMB_LEVEL ml ON ml.memb_level = cm.memb_level AND ml.memb_org_no = cm.memb_org_no
WHERE customer_no = @customer_no
AND current_status = 2
AND ml.memb_org_no = 4 OR ml.memb_org_no = 5

SET @calendar_date = ISNULL(@calendar_date, 
					(SELECT CAST(CONVERT(varchar(10),CURRENT_TIMESTAMP,101) as datetime)))

SELECT
	e.entitlement_no,
	e.memb_level_no,
	mem_org_desc = mo.description,
	mem_level_desc = ml.description,
	entitlement_keyword_desc = kw.description,
	reset_type_desc = gd.description,
	price_type_group = pg.id,
	price_type_group_desc = pg.description,
	num_start_ent = COALESCE(ce.num_ent,e.num_ent),
	num_remain_ent = COALESCE(ce.num_ent,e.num_ent) - ISNULL(coe.cust_num_used,0),
	e.custom_1,
	e.custom_2,
	e.custom_3,
	e.custom_4,
	e.custom_5,
	e.custom_6,
	e.custom_7,
	e.custom_8,
	e.custom_9,
	e.custom_10,
	cm.current_status
FROM dbo.LTR_ENTITLEMENT e
	JOIN dbo.TR_TKW kw ON e.ent_tkw_id = kw.id
	JOIN dbo.T_MEMB_LEVEL ml ON e.memb_level_no = ml.memb_level_no
	JOIN dbo.T_MEMB_ORG mo ON mo.memb_org_no = ml.memb_org_no
	JOIN (
		SELECT
			customer_no,
			cust_memb_no,
			memb_org_no,
			memb_level,
			init_dt,
			expr_dt,
			current_status
		FROM TX_CUST_MEMBERSHIP
		WHERE customer_no = @customer_no AND current_status=2
			) cm
	ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level)
	JOIN dbo.TR_GOOESOFT_DROPDOWN gd ON (gd.code = 1002 AND gd.short_desc = e.reset_type)
	JOIN dbo.TR_PRICE_TYPE_GROUP pg ON pg.id = e.ent_price_type_group
	LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
		ON (e.entitlement_no = ce.entitlement_no 
		AND ce.customer_no = cm.customer_no
		AND ce.cust_memb_no = (SELECT cust_memb_no FROM dbo.TX_CUST_MEMBERSHIP WHERE customer_no=ce.customer_no and current_status=2)
		AND (e.reset_type = 'M' OR e.reset_type='D'
			)
			)
	LEFT JOIN (
		SELECT
			customer_no,
			cust_memb_no,
			entitlement_no, 
			entitlement_date,
			cust_num_used = ISNULL(SUM(num_used),0)
		FROM dbo.LTX_CUST_ORDER_ENTITLEMENT
		GROUP BY customer_no, cust_memb_no, entitlement_no, entitlement_date) coe
	ON (
		coe.customer_no = cm.customer_no 
		AND coe.cust_memb_no = cm.cust_memb_no
		AND coe.entitlement_no = e.entitlement_no
		AND (
			 reset_type = 'M' OR e.reset_type='D'
			)
		)
WHERE cm.customer_no = @customer_no

ORDER BY /* ml.rank,  */ kw.description, pg.description

GO

GRANT EXECUTE ON [dbo].[LWP_GET_CUST_ENTITLEMENTS_EX] TO ImpUsers
GO