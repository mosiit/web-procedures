USE [impresario]

GO
/****   (2015-09-17)    ****/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_GET_TOKENS_FOR_CUSTOMER_FROM_SESSION_KEY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LP_GET_TOKENS_FOR_CUSTOMER_FROM_SESSION_KEY]
GO

USE [impresario]
GO

CREATE PROCEDURE [dbo].[LP_GET_TOKENS_FOR_CUSTOMER_FROM_SESSION_KEY]
    @sessionKey VARCHAR(255) = null
AS
    DECLARE @customerNo int = null, @errmsg varchar(255)

    IF @sessionKey is null OR not exists (SELECT * FROM t_web_session_session WHERE [sessionkey] = @sessionKey)
        BEGIN
            SELECT errmsg = 'Invalid Customer Session ID'
            RAISERROR(@errmsg, 11, 2) WITH SETERROR
            return -101
        END
    ELSE
        SELECT @customerNo = customer_no
            FROM t_web_session_session
            WHERE [sessionkey] = @sessionKey

        SELECT * FROM [dbo].[LTR_SAVE_CARD] WHERE customer_no = @customerNo
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_GET_TOKENS_FOR_CUSTOMER_FROM_SESSION_KEY') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Get Tokens For Customer','LP_GET_TOKENS_FOR_CUSTOMER_FROM_SESSION_KEY')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_GET_TOKENS_FOR_CUSTOMER_FROM_SESSION_KEY'

