USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_UPDATE_GUEST_LOGIN_TYPE] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LWP_UPDATE_GUEST_LOGIN_TYPE]  ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_UPDATE_GUEST_LOGIN_TYPE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_UPDATE_GUEST_LOGIN_TYPE]
GO


CREATE PROCEDURE [dbo].[LWP_UPDATE_GUEST_LOGIN_TYPE]
	@sessionkey VARCHAR(64),
	@login VARCHAR(80)
AS
BEGIN
  DECLARE @errmsg varchar(255);

  SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
  End
	Else
    UPDATE [dbo].[T_CUST_LOGIN]
	  SET login_type = 1
	  WHERE login_type = 3 AND login = @login
  END
GO

GRANT EXECUTE ON [dbo].[LWP_UPDATE_GUEST_LOGIN_TYPE] TO ImpUsers
GO