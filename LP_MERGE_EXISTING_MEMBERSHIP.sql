USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_MERGE_EXISTING_MEMBERSHIP]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_MERGE_EXISTING_MEMBERSHIP]
GO

CREATE PROCEDURE [dbo].[LP_MERGE_EXISTING_MEMBERSHIP]
    @cust_no varchar(120),
    @memb_no varchar(120),
    @cust_email varchar(255),
    @login_type int
AS
    DECLARE @eaddress_no int

    Exec @eaddress_no = [dbo].ap_get_nextid_function 'AD'

    IF NOT EXISTS (SELECT * FROM T_EADDRESS WHERE customer_no = @memb_no AND primary_ind = 'Y')
        BEGIN
            INSERT INTO T_EADDRESS
            (
                eaddress_no,
                customer_no,
                eaddress_type,
                address,
                start_dt,
                end_dt,
                months,
                primary_ind,
                inactive,
                market_ind,
                alt_signor,
                mail_purposes,
                html_ind
            ) VALUES (
                @eaddress_no,
                @memb_no,
                1,
                @cust_email,
                NULL,
                NULL,
                'YYYYYYYYYYYY',
                'Y',
                'N',
                'Y',
                NULL,
                NULL,
                'Y'
            );
        END
    ELSE
        INSERT INTO T_EADDRESS
        (
            eaddress_no,
            customer_no,
            eaddress_type,
            address,
            start_dt,
            end_dt,
            months,
            primary_ind,
            inactive,
            market_ind,
            alt_signor,
            mail_purposes,
            html_ind
        ) VALUES (
            @eaddress_no,
            @memb_no,
            1,
            @cust_email,
            NULL,
            NULL,
            'YYYYYYYYYYYY',
            'N',
            'N',
            'Y',
            NULL,
            NULL,
            'Y'
        );

    UPDATE T_CUST_LOGIN SET customer_no = @memb_no
    WHERE customer_no = @cust_no AND login_type = @login_type AND login = @cust_email;

    SELECT * FROM T_CUST_LOGIN
    WHERE customer_no = @memb_no AND login_type = @login_type AND login = @cust_email;
GO

GRANT EXECUTE ON [dbo].[LP_MERGE_EXISTING_MEMBERSHIP] TO ImpUsers
GO
