USE [impresario]
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_TO_CARD_TO_TEMP_TABLE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_TO_CARD_TO_TEMP_TABLE]
GO

CREATE PROCEDURE [dbo].[LWP_ADD_TO_CARD_TO_TEMP_TABLE]
    @customer_no INT,
    @payment_account_id INT,
    @process_dt DATETIME
	with execute as 'dbo'
AS
BEGIN

  SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  INSERT INTO dbo.LTR_CARD_UPDATES(customer_no, payment_account_id, process_dt) VALUES (@customer_no, @payment_account_id, @process_dt);
END

GO
GRANT EXECUTE ON [dbo].[LWP_ADD_TO_CARD_TO_TEMP_TABLE] TO ImpUsers
GO
