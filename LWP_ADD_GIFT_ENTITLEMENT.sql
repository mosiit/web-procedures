USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LWP_ADD_GIFT_ENTITLEMENT]    Script Date: 6/24/2019 12:02:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tessitura Network Consulting Jon Ballinger
-- Create date: 1/18/2018
-- Description:	Procedure used by web to add a new gift for an entitlement.
-- LWP_ADD_GIFT_ENTITLEMENT 2654150,'C',1,480311
-- =============================================
ALTER PROCEDURE [dbo].[LWP_ADD_GIFT_ENTITLEMENT]
	@customer_no int,
	@gift_status char(1)='C',
	@num_items int,
	@cust_entitlement_id int,	
	@group_gift_code varchar(255) = null,
	@gift_recip_email varchar(80) = null,
	@gift_recip_name varchar(255) = null,
	@gift_message varchar(max) = null
AS
BEGIN
	declare @context varchar(100),
		@num_remain int=0,
		@custom_screen_ctrl int,
		@order_entitlement int,
		@gift_code varchar(255),
		@expr_dt datetime
		
	declare @exhibit_hall_keyword  int 
	select  @exhibit_hall_keyword = default_value
	from T_DEFAULTS where field_name='EXHIBIT_HALL_TKW'

	begin try

		select @context = convert(varchar,@customer_no)
		exec ap_set_context 'CT', @context
	
	
		INSERT INTO LT_ENTITLEMENT_GIFT ( customer_no, gift_status, num_items, ltx_cust_entitlement_id ,group_gift_code,gift_recip_email,gift_recip_name,gift_message) 
		VALUES ( @customer_no, @gift_status, @num_items, @cust_entitlement_id,@group_gift_code,@gift_recip_email,@gift_recip_name,@gift_message )

		
		
		select @order_entitlement = max(id_key) from LTX_CUST_ORDER_ENTITLEMENT  where ltx_cust_entitlement_id = @cust_entitlement_id
		
		
		select @gift_code = gift_code 
		from LTX_CUST_ORDER_ENTITLEMENT where id_key=@order_entitlement
		
		

		select @custom_screen_ctrl = custom_screen_ctrl, @expr_dt=expr_dt
		from LT_ENTITLEMENT_GIFT where gift_code=@gift_code
		
		
		update LT_ENTITLEMENT_GIFT set group_gift_code =coalesce(@group_gift_code,@gift_code) where custom_screen_ctrl=@custom_screen_ctrl

		
		if @group_gift_code is not null
		Begin
		
			if exists(select 1 from 
					LT_ENTITLEMENT_GIFT a
					join LTR_ENTITLEMENT b on b.entitlement_no=a.entitlement_no
					where a.group_gift_code = @group_gift_code 
					and a.customer_no=@customer_no
					and b.ent_tkw_id = @exhibit_hall_keyword)
			begin
				declare @new_group_gift_code varchar(255)
				select top(1)  @new_group_gift_code = gift_code				
				from LT_ENTITLEMENT_GIFT a
					join LTR_ENTITLEMENT b on b.entitlement_no=a.entitlement_no
					where a.group_gift_code = @group_gift_code 
					and a.customer_no=@customer_no
					and b.ent_tkw_id = @exhibit_hall_keyword
				order by a.expr_dt desc, a.id_key
				
				if @new_group_gift_code is not null and @new_group_gift_code!=@group_gift_code
				begin
					update LT_ENTITLEMENT_GIFT set group_gift_code = @new_group_gift_code
					where customer_no=@customer_no
					and group_gift_code =@group_gift_code

					select @group_gift_code=@new_group_gift_code
				end
			end

			--Resetting expirationd date
			declare @init_dt datetime
			select --@expr_dt = max(expr_dt)
			@init_dt=min(init_dt)
			from LT_ENTITLEMENT_GIFT 
			where group_gift_code =@group_gift_code
			and customer_no=@customer_no

			
			update a set 
			--a.expr_dt=@expr_dt,
			init_dt=@init_dt
			from LT_ENTITLEMENT_GIFT  a
			where gift_code =@group_gift_code
			and customer_no=@customer_no

			select @expr_dt = expr_dt
					from LT_ENTITLEMENT_GIFT 
					where gift_code = @group_gift_code
		end
		
		
	end try 
	begin catch
		
		DECLARE @ErrMsg NVARCHAR(4000) 
		SELECT @ErrMsg = ERROR_MESSAGE() 
		RAISERROR(@ErrMsg, 16, 1) 
	end catch

	exec ap_set_context 'CT', ''
	select coalesce(@group_gift_code,@gift_code) as gift_code,@expr_dt as expr_dt

	
END



GO


