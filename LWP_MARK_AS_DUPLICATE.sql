USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_MARK_AS_DUPLICATE]    Script Date: 02/01/2016 09:11:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LWP_MARK_AS_DUPLICATE]    Script Date: 05/30/2014 07:54:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_MARK_AS_DUPLICATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_MARK_AS_DUPLICATE]
GO

CREATE PROCEDURE [dbo].[LWP_MARK_AS_DUPLICATE]
	@sessionkey VARCHAR(64),
	@customer_no INT,
	@email VARCHAR(255)
	with execute as 'dbo'
AS
BEGIN

  DECLARE @errmsg varchar(255);
  DECLARE @keep_cust INT;
  DECLARE @newID INT;

	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End
	Else

	  SELECT TOP 1 @keep_cust = customer_no
	  FROM [dbo].[T_CUST_LOGIN]
	  WHERE login = @email
	    AND inactive = 'N'

    SELECT TOP 1 @newID = criterion + 1
    FROM [dbo].[T_POTENTIAL_DUPS]
    order by criterion desc

		INSERT INTO [dbo].[T_POTENTIAL_DUPS]
    (criterion, customer_no, keep_cust, status, identify_method)
    VALUES (@newID, @customer_no, @keep_cust, 'D', -2);

    INSERT INTO [dbo].[T_POTENTIAL_DUPS]
    (criterion, customer_no, status, identify_method)
    VALUES (@newID, @keep_cust, 'K', -2);

END
