USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_GET_RECOMMENDATIONS]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LWP_GET_RECOMMENDATIONS] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LWP_GET_RECOMMENDATIONS] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LWP_GET_RECOMMENDATIONS]
        @session_key VARCHAR(64) = '',  --Ties to the web/kiosk order
        @recommend_type INT = 1,        --1 = Public / 2 = Members / 3 = Kiosk / 4 = Schools
        @perf_no INT = 0,               --Performance number being added to the order
        @zone_no INT = 0,               --Performance zone being added to the order
        @number_of_tix INT = 1,         --Number of tikets added to order
        @perf_count INT = 1,            --Number of performances *for each production* to return (0 = All Perfs)
        @total_recommendations INT = 0  --Maximum number of rows to return (0 = All)
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Constants, Variables, and Temp Tables  */

        DECLARE @recommend_weight_id INT = 15, @any_time_id INT = 46, @exh_required_id INT = 47;
        DECLARE @exh_title_no INT = 27, @mem_title_no INT = 1126

        DECLARE @exh_perf_no INT = 0
        
        DECLARE @is_exh_perf CHAR(1) = 'N',
                @is_mem_perf CHAR(1) = 'N',
                @order_exh_time VARCHAR(8) = ''

        DECLARE @recommendation_list VARCHAR(255) = '';
        DECLARE @recommend_type_str VARCHAR(25) = CASE WHEN @recommend_type = 4 THEN 'Schools'
                                                       WHEN @recommend_type = 3 THEN 'Kiosk'
                                                       WHEN @recommend_type = 2 THEN 'Members'
                                                       ELSE 'Public' END
               
        DECLARE @perf_date CHAR(10) = '', @perf_time VARCHAR(8) = '', @perf_requires_exh CHAR(1);

        DECLARE @prod_no INT = (SELECT [production_no] 
                                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                WHERE [performance_no] = @perf_no AND [performance_zone] = @zone_no);

        DECLARE @order_info TABLE ([perf_no] INT NOT NULL DEFAULT (0),
                                   [zone_no] INT NOT NULL DEFAULT (0),
                                   [perf_date] CHAR(10) NOT NULL DEFAULT (''),
                                   [perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                   [perf_end] VARCHAR(10) NOT NULL DEFAULT (''),
                                   [prod_no] INT NOT NULL DEFAULT (0),
                                   [prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [title_no] INT NOT NULL DEFAULT (0),
                                   [title_name] VARCHAR(30) NOT NULL DEFAULT (''));

        DECLARE @recommendation_table TABLE ([element_id] INT NOT NULL DEFAULT (0),
                                             [perf_no] INT NOT NULL DEFAULT (0),
                                             [zone_no] INT NOT NULL DEFAULT (0),
                                             [perf_date] CHAR(10) NOT NULL DEFAULT (''),
                                             [perf_time] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [rec_id] INT NULL DEFAULT (0),
                                             [prod_season_no] INT NULL DEFAULT(0),
                                             [prod_season_name] VARCHAR(30) NULL DEFAULT(''),
                                             [prod_no] INT NULL DEFAULT(0),
                                             [prod_name] VARCHAR(30) NULL DEFAULT(''),
                                             [rec_weight] INT NULL DEFAULT(0),
                                             [rec_any_time] CHAR(1) NULL DEFAULT('N'),
                                             [requires_exh]  CHAR(1) NULL DEFAULT('N'));

        IF OBJECT_ID('tempdb..#all_perfs') is not null DROP TABLE [#all_perfs];

        CREATE TABLE [#all_perfs] ([performance_no] INT NOT NULL DEFAULT (0),
                                   [performance_zone] INT NOT NULL DEFAULT (0),
                                   [perf_zone_id] VARCHAR(20) NOT NULL DEFAULT (''),
                                   [performance_code] VARCHAR(15) NOT NULL DEFAULT (''),
                                   [performance_date] CHAR(10) NOT NULL DEFAULT (''),
                                   [performance_Time] VARCHAR(8) NOT NULL DEFAULT (''),
                                   [performance_end_time] VARCHAR(8) NOT NULL DEFAULT (''),
                                   [production_season_no] INT NOT NULL DEFAULT (0),
                                   [production_season_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [production_no] INT NOT NULL DEFAULT (0),
                                   [production_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [title_no] INT NOT NULL DEFAULT (0),
                                   [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [available_tix] INT NOT NULL DEFAULT (0),
                                   [image_url] VARCHAR(255) NOT NULL DEFAULT (''),
                                   [rec_weight] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#recommendations_Detail') is not null DROP TABLE [#recommendations_Detail];
        
        CREATE TABLE [#recommendations_Detail] ([rec_id] INT NOT NULL DEFAULT (0),
                                                [rec_weight] INT NOT NULL DEFAULT (0),
                                                [perf_no] INT NOT NULL DEFAULT (0),
                                                [zone_no] INT NOT NULL DEFAULT (0),
                                                [perf_code] VARCHAR(10) NOT NULL DEFAULT (''),
                                                [perf_time] VARCHAR(8) NOT NULL DEFAULT (''),
                                                [prod_season_no] INT NOT NULL DEFAULT (0),
                                                [prod_season_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                [prod_no] INT NOT NULL DEFAULT (0),
                                                [prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                [image_url] VARCHAR(255) NOT NULL DEFAULT (''),
                                                [requires_exh] CHAR(1) NOT NULL DEFAULT ('N'));

        IF OBJECT_ID('tempdb..#final_list') is not null DROP TABLE [#final_list];
        
        CREATE TABLE [#final_list] ([overall_row] INT NOT NULL DEFAULT (0),
                                    [row_num] INT NOT NULL DEFAULT (0),
                                    [rec_id] INT NOT NULL DEFAULT (0),
                                    [rec_weight] INT NOT NULL DEFAULT (0),
                                    [perf_no] INT NOT NULL DEFAULT (0),
                                    [zone_no] INT NOT NULL DEFAULT (0),
                                    [perf_code] VARCHAR(10) NOT NULL DEFAULT (''),
                                    [perf_time] VARCHAR(8) NOT NULL DEFAULT (''),
                                    [prod_season_no] INT NOT NULL DEFAULT (0),
                                    [prod_season_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [prod_no] INT NOT NULL DEFAULT (0),
                                    [prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [image_url] VARCHAR(255) NOT NULL DEFAULT (''));

    /*  Check Parameters  */

        SELECT @session_key = ISNULL(@session_key,'')

        SELECT @recommend_type = ISNULL(@recommend_type, 1)

        IF @recommend_type NOT IN (1, 2, 3, 4) SELECT @recommend_type = 1
        
        SELECT @perf_no = ISNULL(@perf_no, 0),
               @zone_no = ISNULL(@zone_no, 0)

        IF @perf_no = 0 OR @zone_no = 0 GOTO FINISHED

        SELECT @number_of_tix = ISNULL(@number_of_tix, 1),
               @perf_count = ISNULL(@perf_count, 1)

        SELECT @total_recommendations = ISNULL(@total_recommendations, 0)
        IF @total_recommendations = 0 SELECT @total_recommendations = 100

    /*  Determine Performance Date and Time of the performance added to their order
        If the description on the zone is not a date/time, use the time part of the perf_dt field  */

        SELECT @perf_time = [description]
                            FROM [dbo].[T_ZONE] 
                            WHERE [zone_no] = @zone_no;

        SELECT @perf_date = CONVERT(CHAR(10),[perf_dt],111),
               @perf_time = CASE WHEN ISDATE(@perf_time) = 0 THEN CONVERT(VARCHAR(8),[perf_dt],108)
                                 ELSE @perf_time END
                            FROM [dbo].[T_PERF]
                            WHERE [perf_no] = @perf_no;

/*  
        
        --This code will change the perf date to today if it's a membership being added to the cart
                --7/22/2021: It was decided not to implement this piece of the code at this time
                --           But I am leaving it here commented out in case we decide to implement it later


        SELECT @is_mem_perf = (SELECT CASE WHEN MAX([title_no]) = @mem_title_no THEN 'Y' ELSE 'N' END
                               FROM [dbo].[LT_FUTURE_PERF_CACHE] 
                               WHERE [perf_no] = @perf_no)

        IF @is_mem_perf = 'Y' BEGIN

            --SELECT @perf_no, @perf_date, @perf_time

            SELECT @perf_date = FORMAT(ISNULL(CAST(MIN(perf.[perf_date]) AS DATE),GETDATE()),'yyyy/MM/dd')
                                FROM [dbo].[T_WEB_ORDER] AS word 
                                     LEFT OUTER JOIN [dbo].[T_WEB_SUB_LINEITEM] AS wsli ON wsli.[order_no] = word.[order_no]
                                     LEFT OUTER JOIN [dbo].[LT_FUTURE_PERF_CACHE] AS perf ON perf.[perf_no] = wsli.[perf_no] AND perf.[zone_no]= wsli.[zone_no]
            WHERE word.[sessionkey] = @session_key

        END
        
  */    

        SELECT @perf_requires_exh = CASE WHEN ISNULL(cn1.[value],'N') = 'Y' OR ISNULL(cn2.[value],'N') = 'Y' THEN 'Y' ELSE 'N' END
        FROM [dbo].[T_PERF] AS prf
             INNER JOIN [dbo].[T_PROD_SEASON] AS sea ON sea.[prod_season_no] = prf.[prod_season_no]
             INNER JOIN [dbo].[T_INVENTORY] AS ise ON ise.[inv_no] = sea.[prod_season_no]
             INNER JOIN [dbo].[T_PRODUCTION] AS pro ON pro.[prod_no] = sea.[prod_no]
             INNER JOIN [dbo].[T_INVENTORY] AS ipr ON ipr.[inv_no] = pro.[prod_no]
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn1 ON cn1.[inv_no] = pro.[prod_no] AND cn1.[content_type] = @exh_required_id
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn2 ON cn2.[inv_no] = pro.[title_no] AND cn2.[content_type] = @exh_required_id
        WHERE prf.[perf_no] = @perf_no

    /*  Get all performances for the specific date and store them in a temp table for faster access  */

        INSERT INTO [#all_perfs] ([performance_no], [performance_zone], [perf_zone_id], [performance_code], [performance_date], 
                                  [performance_Time], [performance_end_time], [production_season_no], [production_season_name], 
                                  [production_no], [production_name], [title_no], [title_name], [available_tix], [image_url], [rec_weight])
         SELECT [perf_no],
                [zone_no],
                [perf_zone_id],
                [perf_code],
                [perf_date],
                [perf_time],
                [perf_end_time],
                [prod_season_no],
                [prod_season_name],
                [prod_no],
                [prod_name],
                [title_no],
                [title_name],
                [dbo].[LF_GetCapacity]([perf_no], [zone_no], 'Available'),
                [image_url],
                ISNULL(TRY_CONVERT(INT, cn1.[value]),0) AS [rec_weight]
        FROM [dbo].[LT_FUTURE_PERF_CACHE] AS prf
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn1 ON cn1.[inv_no] = prf.[prod_no] AND cn1.[content_type] = @recommend_weight_id
        WHERE perf_date = @perf_date

    /*  Get the performance number for that day's Exhibit Halls Performance  */

        SELECT @exh_perf_no = MAX([performance_no])
                              FROM [#all_perfs] 
                              WHERE [title_no] = @exh_title_no
                                AND LEFT([production_season_name], 2) <> 'z-'

        SELECT @exh_perf_no = ISNULL(@exh_perf_no, 0)

    /*  Is the Performance Being Added to Order the Exhibit Hall Performance  */

        SELECT @is_exh_perf = CASE WHEN @perf_no = @exh_perf_no THEN 'Y' ELSE 'N' END

    /*  Added 11/28/2020
        Deletes any performance that does not have the Web Sales Mode of Sale (#7) assigned to it  */

        DELETE pf
        FROM [#all_perfs] AS pf
             LEFT OUTER JOIN [dbo].[TX_PERF_PKG_MOS] AS ms ON ms.[perf_no] = pf.[performance_no] 
                                                          AND ms.[MOS] = 7
        WHERE ISNULL(ms.[MOS], 0) = 0

    /*  If perf date is today, remove all performances that have already happened  */

        DELETE FROM [#all_perfs]
        WHERE CAST([performance_date] AS DATE) = CAST(GETDATE() AS DATE)
          AND CAST([performance_time] AS TIME) < CAST(GETDATE() AS TIME)

    /*  Delete performances without enough tickets  */

        DELETE FROM [#all_perfs]
        WHERE [available_tix] < @number_of_tix

    /* Delete Performances that are conflicts with the added performance time  */

        DELETE FROM [#all_perfs]
        WHERE @perf_time between [performance_Time] AND [performance_end_time]

    /*  Get existing order information if there is a session key and use it to remove perfs 
        where the times conflict with what they already have in the order  */
                                 
        IF @session_key <> '' BEGIN

            INSERT INTO @order_info ([perf_no], [zone_no], [perf_date], [perf_time], [perf_end], [prod_no], [prod_name], [title_no], [title_name])
            SELECT ISNULL(perf.[perf_no],0),
                   ISNULL(perf.[zone_no],0),
                   ISNULL(perf.[perf_date],''),
                   ISNULL(perf.[perf_time],''),
                   ISNULL(perf.[perf_end_time],''),
                   ISNULL(perf.[prod_no],0),
                   ISNULL(perf.[prod_name],''),
                   ISNULL(perf.[title_no],0),
                   ISNULL(perf.[title_name],'')
            FROM [dbo].[T_WEB_ORDER] AS word 
                 LEFT OUTER JOIN [dbo].[T_WEB_SUB_LINEITEM] AS wsli ON wsli.[order_no] = word.[order_no]
                 LEFT OUTER JOIN [dbo].[LT_FUTURE_PERF_CACHE] AS perf ON perf.[perf_no] = wsli.[perf_no] AND perf.[zone_no]= wsli.[zone_no]
            WHERE word.[sessionkey] = @session_key;

            SELECT @order_exh_time = MAX([perf_time])
                                     FROM @order_info
                                     WHERE title_no = @exh_title_no

            SELECT @order_exh_time = ISNULL(@order_exh_time, '')

            DELETE FROM [#all_perfs]
            WHERE [production_no] IN (SELECT DISTINCT [prod_no] FROM @order_info);

            WITH [CTE_CONFLICTS] ([perf_zone_id])
            AS (SELECT p.[perf_zone_id]
                FROM [#all_perfs] AS p
                     LEFT OUTER JOIN @order_info AS o ON o.[perf_date] = p.[performance_date] AND p.[performance_time] BETWEEN o.[perf_time] AND o.[perf_end]
                WHERE ISNULL(o.[perf_no],0) > 0)
            DELETE FROM [#all_perfs] 
            WHERE [perf_zone_id] IN (SELECT [perf_zone_id] FROM [CTE_CONFLICTS]);
         
        END

    /*  Get the list of recommendation production seasons from the production being added to the order  */

        SELECT @recommendation_list = ISNULL(cn.[value],'')
        FROM [dbo].[t_perf] AS pf
             INNER JOIN [dbo].[T_PROD_SEASON] AS ps ON ps.[prod_season_no] = pf.[prod_season_no]
             INNER JOIN [dbo].[T_PRODUCTION] AS pr ON pr.[prod_no] = ps.[prod_no]
             INNER JOIN [dbo].[TX_INV_CONTENT] AS cn ON cn.[inv_no] = pr.[prod_no] AND cn.[content_type] = @recommend_type
        WHERE pf.[perf_no] = @perf_no;

    /* Parse the recommendation list and get the specific information for each production season */

        --INSERT INTO @recommendation_table ([perf_no], [zone_no], [perf_date], [perf_time], [rec_id], [prod_season_no], 
        --                                   [prod_season_name], [prod_no], [prod_name], [rec_weight], [rec_any_time], [requires_exh])
        --SELECT @perf_no AS [perf_no],
        --       @zone_no AS [zone_no],
        --       @perf_date AS [perf_date],
        --       @perf_time AS [perf_time],
        --       lis.[ElementId], 
        --       ISNULL(TRY_CONVERT(INT,lis.[Element]),0) AS [prod_season_no],
        --       ise.[description] AS [prod_season_name],
        --       sea.[prod_no],
        --       ipr.[description] AS [prod_name],
        --       ISNULL(TRY_CONVERT(INT, cn1.[value]),0) AS [rec_weight],
        --       CAST(ISNULL(cn2.[value],'*') AS VARCHAR(10)) AS [rec_any_time],
        --       CAST(CASE WHEN ISNULL(cn3.[value],'N') = 'Y' OR ISNULL(cn4.[value],'N') = 'Y' THEN 'Y' ELSE 'N' END AS VARCHAR(10)) AS [requires_exh]
        --FROM [dbo].[FT_SPLIT_LIST](@recommendation_list,',') AS lis
        --     INNER JOIN [dbo].[T_PROD_SEASON] AS sea ON sea.[prod_season_no] = lis.[Element]
        --     INNER JOIN [dbo].[T_INVENTORY] AS ise ON ise.[inv_no] = sea.[prod_season_no]
        --     INNER JOIN [dbo].[T_PRODUCTION] AS pro ON pro.[prod_no] = sea.[prod_no]
        --     INNER JOIN [dbo].[T_INVENTORY] AS ipr ON ipr.[inv_no] = pro.[prod_no]
        --     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn1 ON cn1.[inv_no] = pro.[prod_no] AND cn1.[content_type] = @recommend_weight_id
        --     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn2 ON cn2.[inv_no] = pro.[prod_no] AND cn2.[content_type] = @any_time_id
        --     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn3 ON cn3.[inv_no] = pro.[prod_no] AND cn3.[content_type] = @exh_required_id
        --     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn4 ON cn4.[inv_no] = pro.[title_no] AND cn4.[content_type] = @exh_required_id
        --ORDER BY ISNULL(TRY_CONVERT(INT, cn1.[value]),0);


    --SELECT * FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]  WHERE [inactive] = 'N' AND [rec_type] = @recommend_type_str AND  [production] = @prod_no ORDER BY [rec_weight];

    WITH CTE_RECOMMENDATIONS ([rec_id], [prod_no], [rec_prod_no])
    AS (SELECT ROW_NUMBER() OVER(ORDER BY production DESC), 
               [production], 
               [recommendation]
        FROM (SELECT [production],      [rec_prod_01],          [rec_prod_02],          [rec_prod_03], 
                     [rec_prod_04],     [rec_prod_05],          [rec_prod_06],          [rec_prod_07], 
                     [rec_prod_08],     [rec_prod_09],          [rec_prod_10]           
              FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] 
              WHERE [inactive] = 'N' 
                AND CAST(@perf_date AS DATE) BETWEEN [start_dt] AND [end_dt]
                AND [rec_type] = @recommend_type_str 
                AND  [production] = @prod_no) p
              UNPIVOT ([recommendation] FOR [rec_prod_id] IN (rec_prod_01, rec_prod_02, rec_prod_03, rec_prod_04, rec_prod_05, rec_prod_06, rec_prod_07, rec_prod_08, rec_prod_09, rec_prod_10)) AS unpvt)
        INSERT INTO @recommendation_table ([perf_no], [zone_no], [perf_date], [perf_time], [rec_id], [prod_season_no], 
                                           [prod_season_name], [prod_no], [prod_name], [rec_weight], [rec_any_time], [requires_exh])
        SELECT DISTINCT @perf_no AS [perf_no],
               @zone_no AS [zone_no],
               @perf_date AS [perf_date],
               @perf_time AS [perf_time],
               cte.[rec_id] AS [ElementId],
               prf.[production_season_no] AS [prod_season_no],
               prf.[production_season_name] AS [prod_season_name],
               prf.[production_no] AS [prod_no],
               prf.[production_name] AS [prod_name],
               prf.[rec_weight] AS [rec_weight],
               CAST(ISNULL(cn2.[value],'*') AS VARCHAR(10)) AS [rec_any_time],
               CAST(CASE WHEN ISNULL(cn3.[value],'N') = 'Y' OR ISNULL(cn4.[value],'N') = 'Y' THEN 'Y' ELSE 'N' END AS VARCHAR(10)) AS [requires_exh]
        FROM [CTE_RECOMMENDATIONS] AS cte
             INNER JOIN [#all_perfs] AS prf ON prf.[performance_date] = @perf_date AND prf.[production_no] = cte.[rec_prod_no]
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn2 ON cn2.[inv_no] = cte.[rec_prod_no] AND cn2.[content_type] = @any_time_id
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn3 ON cn3.[inv_no] = cte.[rec_prod_no] AND cn3.[content_type] = @exh_required_id
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cn4 ON cn4.[inv_no] = prf.[title_no] AND cn4.[content_type] = @exh_required_id
        ORDER BY cte.[rec_id]

    /*  Get the Recommendation Details  */
 
        INSERT INTO [#recommendations_Detail] ([rec_id], [rec_weight], [perf_no], [zone_no], [perf_code], [perf_time], [prod_season_no], 
                                               [prod_season_name],[prod_no],[prod_name], [image_url], [requires_exh])
        SELECT rec.[rec_id],
               rec.[rec_weight],
               prf.[performance_no],
               prf.[performance_zone],
               prf.[performance_code],
               prf.[performance_Time],
               prf.[production_season_no],
               prf.[production_season_name],
               prf.[production_no],
               prf.[production_name],
               prf.[image_url],
               rec.[requires_exh]
        FROM [#all_perfs] AS prf
             INNER JOIN @recommendation_table AS rec ON rec.[prod_season_no] = prf.[production_season_no]
        WHERE [prf].[performance_date] = @perf_date
        ORDER BY rec.[rec_id]

    /*  Never recommend the same performance as what is being added to the order  */

        DELETE FROM [#recommendations_Detail] WHERE [perf_no] = @perf_no

    /*  If the perf being added to the order is the Exh Hall Performance,
        Delete anything that requires an Exh Hall Admission and starts before the Exh Hall time  */

        IF @is_exh_perf = 'Y'
            DELETE FROM [#recommendations_Detail] WHERE [requires_exh] = 'Y' AND [perf_time] <= @perf_time
 
    /*  If the perf being added to the order requires Exh Hall admission  
        Delete anything Exh Hall performance with a timer after the added perf time  */

        IF @perf_requires_exh = 'Y'
            DELETE FROM [#recommendations_Detail] WHERE [perf_no] = @exh_perf_no AND perf_time >= @perf_time

    /*  If the Order Exh Time is not blank (meaning Exhibit Halls are already on the order)  
        Delete anything that requires Exh Hall admission and starts before the order Exh Hall time  */

        IF @order_exh_time <> ''
            DELETE FROM [#recommendations_Detail] WHERE [perf_no] <> @exh_perf_no AND [requires_exh] = 'Y' AND perf_time <= @order_exh_time

    /*  Get final list of performances to recommend  */
    
        INSERT INTO [#final_list] ([overall_row], [row_num], [rec_id], [rec_weight], [perf_no], [zone_no], [perf_code], 
                                    [perf_time], [prod_season_no], [prod_season_name],[prod_no],[prod_name], [image_url])
        SELECT ROW_NUMBER() OVER (ORDER BY [rec_id] DESC) AS [overall_row],
               ROW_NUMBER() OVER(PARTITION BY [rec_weight], [prod_name] 
                                 ORDER BY [rec_weight] DESC, [prod_name] ASC, [perf_Time] ASC) AS [row_num],
               [rec_id],
               [rec_weight],
               [perf_no],
               [zone_no],
               [perf_code],
               [perf_Time],
               [prod_season_no],
               [prod_season_name],
               [prod_no],
               [prod_name],
               [image_url]
        FROM [#recommendations_Detail]
        ORDER BY [rec_id] DESC, [row_num]


    /*  Delete extra rows based on the number of perfs for each production recommended that have been asked for  */
        
        IF @perf_count > 0
            DELETE FROM [#final_list] WHERE [row_num] > @perf_count

    FINISHED:

        --SELECT * FROM [#final_list] ORDER BY [overall_row] DESC

        /*  Final listing  */

        SELECT TOP (@total_recommendations) 
               [overall_row] as [rec_weight],
               [perf_no],
               [zone_no],
               [perf_code],
               [prod_season_no],
               [prod_no],
               [prod_name],
               [perf_time],
               [image_url]
        FROM [#final_list] 
        ORDER BY [overall_row] DESC, [perf_time]

    DONE:

        IF OBJECT_ID('tempdb..#final_list') is not null DROP TABLE [#final_list];
        IF OBJECT_ID('tempdb..#recommendation_detail') is not null DROP TABLE [#recommendation_table];
        IF OBJECT_ID('tempdb..#all_perfs') is not null DROP TABLE [#all_perfs];

END;
GO

/*   FOR TESTING

--DECLARE @perf INT = 87443, @zone INT = 733, @session VARCHAR(64) = ''
DECLARE @perf INT = 86618, @zone INT = 106, @session VARCHAR(64) = ''
--SELECT @session = '9840d62c050611eb81020050569570d700000000000000000000000000000000'

EXECUTE [dbo].[LWP_GET_RECOMMENDATIONS]
        @session_key = @session,
        @recommend_type = 1,        --1 = Public / 2 = Members / 3 = Kiosk / 4 = Schools
        @perf_no = @perf, 
        @zone_no = @zone,
        @number_of_tix = 3,         --Number of tikets added to order
        @perf_count = 1,            --Number of performances for each production to return (0 = All Perfs)
        @total_recommendations = 5

*/

/*    FOR TESTING

DECLARE @prfDate char(10) = '2021/07/11'
DECLARE @exhTime varchar(8) = '14:15', @pixTime varchar(8) = '15:00', @plaTime varchar(8) = '15:30'

SELECT performance_no, performance_zone, title_name, production_name, performance_time
FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
WHERE performance_date = @prfDate AND [title_name] = 'Special Exhibitions' AND performance_time = @pixTime

UNION SELECT performance_no, performance_zone, title_name, production_name, performance_time
FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
WHERE performance_date = @prfDate AND [title_name] = 'Hayden Planetarium' and performance_time = @plaTime

UNION SELECT performance_no, performance_zone, title_name, production_name, performance_time
FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
WHERE performance_date = @prfDate AND [title_name] = 'Exhibit Halls' and performance_time = @exhTime

UNION SELECT performance_no, performance_zone, title_name, production_name, performance_time
FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
WHERE DATEPART(month,performance_dt) = datepart(month,CAST(@prfDate as date)) and DATEPART(year,performance_dt) = datepart(year,CAST(@prfDate as date)) AND [title_name] = 'Membership' --and performance_time = @exhTime

--SELECT * FROM [dbo].[LT_FUTURE_PERF_CACHE]
--SELECT * FROM [dbo].[T_WEB_ORDER] ORDER BY [order_dt] DESC


--SELECT @session_key = '9840d62c050611eb81020050569570d700000000000000000000000000000000',
--       @recommend_type = 1,        --1 = Public / 2 = Members / 3 = Kiosk / 4 = Schools
--       @perf_no = 87441, 
--       @zone_no = 733,
--       @number_of_tix = 3,         --Number of tikets added to order
--       @perf_count = 1,            --Number of performances for each production to return (0 = All Perfs)
--       @total_recommendations = 5

*/
