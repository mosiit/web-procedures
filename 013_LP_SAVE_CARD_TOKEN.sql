USE [impresario]

GO
/****   (2015-09-17)    ****/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_SAVE_CARD_TOKEN]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_SAVE_CARD_TOKEN]
GO

CREATE PROCEDURE [dbo].[LP_SAVE_CARD_TOKEN]
    @sessionKey VARCHAR(255) = null,
    @token VARCHAR(50) = null,
    @cardNumber VARCHAR(4) = null,
    @expiryMonth VARCHAR(2) = null,
    @expiryYear VARCHAR(2) = null,
    @cardType VARCHAR(20) = null,
    @cardInactive CHAR(1) = "N"
AS BEGIN

    -- @lastNumber will be the last 4 digits of the card number
    DECLARE @customerNo INT = null, @errmsg VARCHAR(255), @lastDigits VARCHAR(4) = SUBSTRING(@cardNumber, LEN(@cardNumber) - 3, 4)

    IF @sessionKey is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @sessionKey) BEGIN

        SELECT errmsg = 'Invalid Customer Session ID'
        RAISERROR(@errmsg, 11, 2) WITH SETERROR
        return -101
    
    END ELSE BEGIN

      SELECT @customerNo = customer_no FROM t_web_session_session WHERE sessionkey = @sessionKey

	IF NOT EXISTS (
	   SELECT *
		FROM LTR_SAVE_CARD
		WHERE customer_no = @customerNo AND last_digit = @lastDigits AND expiry_month = @expiryMonth AND expiry_year = @expiryYear AND card_type = @cardType
	)

		BEGIN
			INSERT INTO
				[impresario].[dbo].[LTR_SAVE_CARD] (customer_no, payment_token, last_digit, expiry_month, expiry_year, card_type, inactive)
				VALUES (@customerNo, @token, @lastDigits, @expiryMonth, @expiryYear, @cardType, @cardInactive)
		END

    END

END
GO


GRANT EXECUTE ON [dbo].[LP_SAVE_CARD_TOKEN] TO impusers
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_SAVE_CARD_TOKEN') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Save Card Token','LP_SAVE_CARD_TOKEN')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_SAVE_CARD_TOKEN'


