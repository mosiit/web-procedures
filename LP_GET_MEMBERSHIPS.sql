USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_GET_MEMBERSHIPS]
    @session_key varchar(64) = null
AS
    SET NOCOUNT ON

    DECLARE @customer_nos TABLE(Value BIGINT)
    DECLARE @customer_no int = null,
        @errmsg varchar(255)

    IF @session_key is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @session_key)
        BEGIN
            SELECT errmsg = 'Invalid Customer Session ID'
            RAISERROR(@errmsg, 11, 2) WITH SETERROR
            return -101
        END
    ELSE
        SELECT @customer_no = customer_no
        FROM t_web_session_session
        WHERE sessionkey = @session_key

        INSERT INTO @customer_nos VALUES (@customer_no)

        INSERT INTO @customer_nos(Value) SELECT group_customer_no FROM [dbo].[T_AFFILIATION] WHERE individual_customer_no = @customer_no

        SELECT
            cust_membership.memb_org_no,
            cust_membership.current_status,
            cust_membership.memb_level,
            cust_membership.init_dt,
            cust_membership.expr_dt,
            cust_membership.inception_dt,
            cust_membership.ben_provider,
            cust_membership.declined_ind,
            cust_membership.cust_memb_no,
            cust_membership.recog_amt,
            cust_membership.memb_amt,
            cust_membership.AVC_amt,
            cust_membership.NRR_status,
            cust_membership.customer_no,
            current_status.description current_status_desc,
            memb_level.description memb_level_desc,
            memb_level.category memb_level_category,
            memb_level.renewal renewal_period,
            category.description category_desc,
            memb_org.description memb_org_desc,
            memb_trend.description,
            cat_trend.description
        FROM [impresario].[dbo].[TX_CUST_MEMBERSHIP] cust_membership
        LEFT OUTER JOIN [impresario].[dbo].[T_MEMB_LEVEL] memb_level on cust_membership.memb_level = memb_level.memb_level
        LEFT OUTER JOIN [impresario].[dbo].[TR_MEMB_LEVEL_CATEGORY] category ON memb_level.category = category.id
        LEFT OUTER JOIN [impresario].[dbo].[TR_CURRENT_STATUS] current_status ON cust_membership.current_status = current_status.id
        JOIN [impresario].[dbo].[T_MEMB_ORG] memb_org ON cust_membership.memb_org_no = memb_org.memb_org_no
        LEFT OUTER JOIN [impresario].[dbo].[TR_MEMB_TREND] memb_trend ON memb_trend.id = cust_membership.memb_trend
        LEFT OUTER JOIN [impresario].[dbo].[TR_MEMB_TREND] cat_trend ON cat_trend.id = cust_membership.category_trend
        WHERE
            (cust_membership.customer_no = @customer_no)
            OR (
                --  Only show Groups memberships if they are Household membership
                cust_membership.customer_no IN (SELECT * FROM @customer_nos)
                AND
                cust_membership.memb_org_no = 4
            )
