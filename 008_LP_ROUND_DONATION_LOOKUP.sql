USE [impresario]

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_ROUND_DONATION_LOOKUP]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_ROUND_DONATION_LOOKUP]
GO

USE [impresario]
GO

CREATE PROCEDURE [dbo].[LP_ROUND_DONATION_LOOKUP]
    @session_key varchar(64) = null,
-- UPDATE FROM TASK 511 START
    @basket_amount float = null,
    @variant_id int = 1
-- UPDATE FROM TASK 511 END
AS BEGIN

    SET NOCOUNT ON

    DECLARE @customer_no int = null, @errmsg varchar(255)

    IF @session_key is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @session_key)
        BEGIN
            SELECT errmsg = 'Invalid Customer Session ID'
            RAISERROR(@errmsg, 11, 2) WITH SETERROR
            return -101
        END
    ELSE
        SELECT @customer_no = customer_no FROM t_web_session_session WHERE sessionkey = @session_key

        SELECT TOP 1 * FROM [dbo].[LTR_WEB_DONATION_CONFIG_ROUND]
        WHERE
-- UPDATE FROM TASK 511 START
            (variant_id = @variant_id)
            AND
-- UPDATE FROM TASK 511 END
            (range_start <= @basket_amount)
            AND
            (
                (range_end >= @basket_amount)
                OR
                (range_end IS NULL)
            )
        ORDER BY range_start ASC;
END
GO

GRANT EXECUTE ON [dbo].[LP_ROUND_DONATION_LOOKUP] TO impusers
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_ROUND_DONATION_LOOKUP') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Round Donation Lookup','LP_ROUND_DONATION_LOOKUP')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_ROUND_DONATION_LOOKUP'

