USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_MEMBERSHIP_EX]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_MEMBERSHIP_EX]
GO


CREATE PROCEDURE [dbo].[LWP_ADD_MEMBERSHIP_EX]
	@sessionkey VARCHAR(64),
	@fyear INT,
	@category INT,
	@membershipOrgLevel INT,
	@membershipLevel VARCHAR (3)
AS
BEGIN
	
	
	DECLARE @customer_no int, @id_key int, @errmsg varchar(255), @cust_memb_no int, @campaign int

	-- validate sessionkey parameter
	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End
	Else
	    Select @campaign = campaign_no from t_campaign where memb_org_no = @membershipOrgLevel and category = @category and fyear = @fyear
		Select @customer_no = customer_no from t_web_session_session where sessionkey = @sessionkey

	Exec @cust_memb_no = [dbo].ap_get_nextid_function 'CM'

			Insert	[dbo].tx_cust_membership(
				cust_memb_no,
				parent_no,
				customer_no,
				campaign_no,
				memb_org_no,
				memb_level,
				memb_amt,
				ben_provider,
				init_dt,
				expr_dt,
				current_status,
				NRR_status,
				memb_trend,
				declined_ind,
				AVC_amt,
				orig_expiry_dt,
				orig_memb_level,
				inception_dt,
				mir_lock,
				cur_record,
				recog_amt,
				category_trend,
				first_issue_id,
				final_issue_id,
				num_copies)
			Values(	@cust_memb_no,
				0,
				@customer_no,
				@campaign,
				@membershipOrgLevel,
				@membershipLevel,
				0,
				0,		-- replaces "0", so we can create a gift membership FP1525.
				GETDATE(),
				DATEADD(yyyy, 1, GETDATE()),
				2,				-- New memberships are always active.
				'NE',	-- new
				4,
				'N',
				0,
				DATEADD(yyyy, 1, GETDATE()),
				@membershipLevel,
				GetDate(),		-- replaces above CWR 4/19/2010 FP1560.
				0,
				'Y',
				0,
				4,
				NULL,
				NULL,
				NULL)
END
GO
