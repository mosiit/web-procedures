USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_GET_SCANNED_ORDERS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_GET_SCANNED_ORDERS]
GO

CREATE PROCEDURE [dbo].[LWP_GET_SCANNED_ORDERS]
	@sessionkey VARCHAR(64),
	@order_no INT
AS
BEGIN

  DECLARE @errmsg varchar(255);

	-- validate sessionkey parameter
	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End

	SELECT * FROM [dbo].[t_order_seat_hist]
	WHERE event_code = 22
	  AND order_no = @order_no
		
END

GRANT EXECUTE ON [dbo].[LWP_GET_SCANNED_ORDERS] TO impusers
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LWP_GET_SCANNED_ORDERS') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Get Scanned Orders','LWP_GET_SCANNED_ORDERS')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LWP_GET_SCANNED_ORDERS'
