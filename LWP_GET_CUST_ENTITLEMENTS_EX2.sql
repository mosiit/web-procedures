USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LWP_GET_CUST_ENTITLEMENTS_EX2]    Script Date: 7/9/2019 11:55:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









ALTER PROCEDURE [dbo].[LWP_GET_CUST_ENTITLEMENTS_EX2]

@sessionkey VARCHAR(64) = NULL,
@customer_no INT = NULL,
@calendar_date DATETIME = NULL,
@reset_type CHAR(1) = 'A' /* D=Daily; M=Membership; A (or null)=All */

AS

SET NOCOUNT ON

/************************************************************************
3/25/2015 Michael Reisman
Retrieves list of current entitlements for customer, for web use

--customer given
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS_ex2 @customer_no = 3743110, @calendar_date = NULL, @reset_type = 'A'
--customer not given
EXEC dbo.LWP_GET_CUST_ENTITLEMENTS @sessionkey  = '9BYH7XQHX8H1IEJ283XLXHDBM29WPDUQE029WW32Q0XVWWDBB823OYOU5OC80029', @calendar_date = NULL, @reset_type = 'A'

Modified
4/28/2015 Splinter code for MFA
4/30/2015 Includes Cart memberships and logic to determine if the new memberhsip will be pending or upgrade
5/9/2015 MIR picking a @cart_memb_level_no at random if there are more than one.
5/12/2015 MIR when finding cart memberships, ignore where fund # is blank.
5/13/2015 MIR using max fund if there are two with the same amt.
6/22/2015 MIR inactive status should not be granted entitlements - only (2,3)
6/26/2015 MIR includes one more column -- current status -- to handle pendings vs actives later. 
5/10/2019 Jon Ballinger & Aileen Duffy-Brown -- Added init date on entitlement so web code can evaluate it for non member entitlements and gifted entitlements.
7/3/2019  Aileen Duffy-Brown added additional fields for Made Media use.
************************************************************************/

DECLARE @errmsg varchar(200)
DECLARE @cart_order_no int
DECLARE @cart_memb_level_no int
DECLARE @cart_cont_amt money
DECLARE @cart_fund_no int

If @sessionkey is NOT NULL AND not exists (select * from [dbo].t_web_session_session where sessionkey = @sessionkey)
  Begin
	select @errmsg = 'Invalid Session ID'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	RETURN -101
  END

IF @sessionkey IS NOT NULL
  BEGIN
	/* get the order and customer */
	SELECT
		@cart_order_no = order_no,
		@customer_no = customer_no
	FROM T_WEB_ORDER
	WHERE sessionkey = @sessionkey

	/* Get the in-cart membership from data in T_WEB_CONTRIBUTION. There can be more than one contribution at one time in the cart, take highest amt */
	SET @cart_cont_amt = (SELECT MAX(contribution_amt) FROM T_WEB_CONTRIBUTION wc WHERE order_no = @cart_order_no AND fund_no IS NOT NULL) -- 5/12/2015
	SET @cart_fund_no = (SELECT MAX(fund_no) FROM dbo.T_WEB_CONTRIBUTION WHERE order_no = @cart_order_no AND contribution_amt = @cart_cont_amt AND fund_no is NOT NULL) --5/12/2015

	SELECT @cart_memb_level_no = c.memb_level_no /* picking one at random: fix 5/9/2015 */
	FROM TX_CAMP_FUND a
		JOIN T_CAMPAIGN b ON a.campaign_no = b.campaign_no
		JOIN T_MEMB_LEVEL c ON c.memb_org_no = b.memb_org_no
	WHERE fund_no = @cart_fund_no
	AND CURRENT_TIMESTAMP BETWEEN a.start_dt AND a.end_dt
	AND @cart_cont_amt BETWEEN c.start_amt AND c.end_amt
	AND b.memb_org_no IS NOT NULL
  END

/* Find existing current membership */
SELECT
	cm.memb_org_no,
	init_dt,
	expr_dt,
	ml.renewal,
	ml.expiry,
	cm.current_status
INTO #active_membs
FROM TX_CUST_MEMBERSHIP cm
	JOIN T_MEMB_LEVEL ml ON ml.memb_level = cm.memb_level AND ml.memb_org_no = cm.memb_org_no
WHERE customer_no = @customer_no
AND current_status IN (2, 3)
AND ml.memb_org_no IN (4, 5)

SET @calendar_date = ISNULL(@calendar_date,
					(SELECT CAST(CONVERT(varchar(10),CURRENT_TIMESTAMP,101) as datetime)))

/*

	ce.id_key,
	customer_no = ce.customer_no,
	memb_level = ml.description + CASE cm.current_status WHEN 2 THEN ' (Active)' WHEN 3 THEN ' (Pending)' ELSE '' END,
	entitlement_desc=e.entitlement_desc,
	entitlement_no=ce.entitlement_no,
	entitlement_type = kw.description,
	entitlement_key = COALESCE(f.perf_code + ' ' + CONVERT(VARCHAR(10),f.perf_dt,101),CONVERT(VARCHAR(10),ce.entitlement_date,101)),
	entitlement_price_type = pt.description,
	num_ent = ce.num_ent,
	num_used = ISNULL(coe.num_used,0),
	num_remain = ce.num_ent - ISNULL(coe.num_used,0),
	last_updated_by = ce.last_updated_by,
	last_update_dt = ce.last_update_dt,
	inactive =ce.inactive,
	note=ce.note,
	init_dt=ce.init_dt,
	expr_dt=	ce.expr_dt,
given_by_customer_no = dn.customer_no,
	given_by_customer_name = dn.display_name,
	given_by_order_no = li.order_no,
	e.is_adhoc
	*/


SELECT
	e.entitlement_no,
	ce.customer_no,
	memb_level_no = cm.cust_memb_no ,
	--mem_org_desc = mo.description,
	mem_level_desc = ml.description,
	entitlement_keyword_desc = kw.description,
	reset_type_desc = gd.description,
	price_type_group = pt.id,
	price_type_group_desc = pt.description,
	num_start_ent = ce.num_ent,
	num_remain_ent = ce.num_ent - (ISNULL(coe.num_used,0) + ISNULL(coe_b.num_used,0)),
	e.custom_1,
	e.custom_2,
	e.custom_3,
	e.custom_4,
	e.custom_5,
	e.custom_6,
	e.custom_7,
	e.custom_8,
	e.custom_9,
	e.custom_10,
	cm.current_status,
	e.transferrable,
	ce.id_key ltx_cust_entitlement_id,
	COALESCE(ce.init_dt, cm.init_dt) as init_dt, --added 5/10/2019 --coalesce added 6/26/19
	COALESCE(ce.expr_dt, cm.expr_dt) as expr_dt, --added 11/7/2018 aduffy-brown for Zendesk ticket # 15290 --coalesce added 6/26/19
	e.date_to_compare,
	--e.entitlement_desc, -- added 7/3/2019 for Made Media Use aileen --7/3/2019 10:10am removed per MOS
	e.is_adhoc, -- added 7/3/2019 for Made Media Use aileen
	g.id_key -- added 7/3/2019 for Made Media Use aileen
FROM dbo.LTX_CUST_ENTITLEMENT ce
	JOIN dbo.LTR_ENTITLEMENT e ON ce.entitlement_no = e.entitlement_no
		AND ce.customer_no = @customer_no		 
	JOIN dbo.TR_PRICE_TYPE_GROUP pt ON pt.id = e.ent_price_type_group
	JOIN dbo.TR_TKW kw ON kw.id = e.ent_tkw_id
	LEFT JOIN dbo.TR_GOOESOFT_DROPDOWN gd ON (gd.code = 1002 AND gd.short_desc = e.reset_type)
	LEFT JOIN dbo.T_MEMB_LEVEL ml ON ml.memb_level_no = e.memb_level_no
	LEFT JOIN dbo.TX_CUST_MEMBERSHIP cm ON (cm.cust_memb_no = ce.cust_memb_no)
	LEFT JOIN T_PERF f ON f.perf_no = ce.entitlement_perf_no
    LEFT JOIN (
    SELECT
                lcoe.customer_no,
                coalesce(lcoe.cust_memb_no,0 ) cust_memb_no,
                lcoe.entitlement_no,            
                entitlement_key = CASE
                            WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
                            WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
                            WHEN le.reset_type = 'M' THEN NULL
                            ELSE NULL
                            END,
                ltx_cust_entitlement_id,
                num_used = ISNULL(SUM(lcoe.num_used),0)
            FROM dbo.LTX_CUST_ORDER_ENTITLEMENT lcoe
                JOIN dbo.LTR_ENTITLEMENT le ON lcoe.entitlement_no = le.entitlement_no
                and lcoe.customer_no=@customer_no
                and ltx_cust_entitlement_id is not null
            GROUP BY lcoe.customer_no,coalesce(lcoe.cust_memb_no,0), lcoe.entitlement_no,       ltx_cust_entitlement_id,
            CASE
                                            WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
                                            WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
                                            WHEN le.reset_type = 'M' THEN NULL
                                            ELSE NULL
                                            END) coe
            ON (
            isnull(coe.ltx_cust_entitlement_id,0) =ce.id_key
            )	    
		
 LEFT JOIN (
SELECT
                lcoe.customer_no,
                coalesce(lcoe.cust_memb_no,0 ) cust_memb_no,
                lcoe.entitlement_no,            
                entitlement_key = CASE
                            WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
                            WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
                            WHEN le.reset_type = 'M' THEN NULL
                            ELSE NULL
                            END,
            coalesce(lcoe.ltx_cust_entitlement_id,-1) ltx_cust_entitlement_id, 
                num_used = ISNULL(SUM(lcoe.num_used),0)
            FROM dbo.LTX_CUST_ORDER_ENTITLEMENT lcoe
                JOIN dbo.LTR_ENTITLEMENT le ON lcoe.entitlement_no = le.entitlement_no
                and lcoe.customer_no=@customer_no
                and ltx_cust_entitlement_id is null				
            GROUP BY lcoe.customer_no,coalesce(lcoe.cust_memb_no,0), lcoe.entitlement_no,coalesce(lcoe.ltx_cust_entitlement_id,-1),
            CASE
                                            WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
                                            WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
                                            WHEN le.reset_type = 'M' THEN NULL
                                            ELSE NULL
                                            END) coe_b
            ON (
            (ISNULL(coe_b.ltx_cust_entitlement_id,0) =ce.id_key) OR --AAD 4/9/2019 see comment above
            (
            ISNULL(coe_b.customer_no,0)= ISNULL(ce.customer_no,0)
            AND ISNULL(coe_b.cust_memb_no,0) = ISNULL(cm.cust_memb_no,0)
            AND coe_b.entitlement_no = e.entitlement_no
            AND (
                (e.reset_type = 'D' AND coe_b.entitlement_key = CONVERT(VARCHAR(10),ce.entitlement_date,101)) /* Bug fix 2/3/2016 */
                OR (e.reset_type = 'P' AND coe_b.entitlement_key = CAST(ce.entitlement_perf_no AS VARCHAR(255))) /* 3/16/2015 */
                OR e.reset_type= 'M'                
                ))
            and coalesce(coe_b.ltx_cust_entitlement_id ,-1)=-1 --AAD 3/27/2019 see comment above
            )
LEFT JOIN dbo.LT_ENTITLEMENT_GIFT g 
            JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() dn ON dn.customer_no = g.customer_no
                ON g.id_key = ce.gift_no
left join  dbo.T_LINEITEM li (NOLOCK) ON li.li_seq_no = g.li_seq_no
left join LT_ENTITLEMENT_SET ls on ls.set_no = ce.set_no
WHERE (
    (is_adhoc='N'  and ce.cust_memb_no>0) and CURRENT_TIMESTAMP  BETWEEN cm.init_dt AND cm.expr_dt 
    OR (cm.current_status = 3 AND CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,0-ml.renewal,cm.init_dt) AND cm.expr_dt) --pending 
    ) 
    or
    (is_adhoc='Y' and coalesce(ce.cust_memb_no,0)=0)
 or (is_adhoc='N' and coalesce(ce.cust_memb_no,0)=0)
 ORDER BY /* ml.rank,  */ kw.description, pt.description
















GO


