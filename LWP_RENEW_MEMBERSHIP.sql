USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_RENEW_MEMBERSHIP]    Script Date: 02/01/2016 09:11:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LWP_RENEW_MEMBERSHIP]    Script Date: 05/30/2014 07:54:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_RENEW_MEMBERSHIP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_RENEW_MEMBERSHIP]
GO


CREATE PROCEDURE [dbo].[LWP_RENEW_MEMBERSHIP]
	@sessionkey VARCHAR(64),
	@customerNo INT,
	@fyear INT,
	@category INT,
	@membershipOrgLevel INT,
	@membershipLevel VARCHAR (3),
	@initialiseDate DATE,
	@current_status INT
AS
BEGIN

	SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @id_key int, @errmsg varchar(255), @cust_memb_no int, @campaign int

	If @initialiseDate is null OR @initialiseDate = ''
	Begin
		SET @initialiseDate = GETDATE()
	End

	-- validate sessionkey parameter
	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End
	Else
	    Select @campaign = campaign_no from t_campaign where memb_org_no = @membershipOrgLevel and category = @category and fyear = @fyear

	Exec @cust_memb_no = [dbo].ap_get_nextid_function 'CM'

			Insert	[dbo].tx_cust_membership(
				cust_memb_no,
				parent_no,
				customer_no,
				campaign_no,
				memb_org_no,
				memb_level,
				memb_amt,
				ben_provider,
				init_dt,
				expr_dt,
				current_status,
				NRR_status,
				memb_trend,
				declined_ind,
				AVC_amt,
				orig_expiry_dt,
				orig_memb_level,
				inception_dt,
				mir_lock,
				cur_record,
				recog_amt,
				category_trend,
				first_issue_id,
				final_issue_id,
				num_copies)
			Values(
				@cust_memb_no,
				0,
				@customerNo,
				@campaign,
				@membershipOrgLevel,
				@membershipLevel,
				0,
				0,		-- replaces "0", so we can create a gift membership FP1525.
				@initialiseDate,
				DATEADD(yyyy, 1, @initialiseDate),
				@current_status,
				'NE',	-- new
				4,
				'N',
				0,
				DATEADD(yyyy, 1, GETDATE()),
				@membershipLevel,
				GetDate(),		-- replaces above CWR 4/19/2010 FP1560.
				0,
				'Y',
				0,
				4,
				NULL,
				NULL,
				NULL)
END

GO
GRANT EXECUTE ON [dbo].[LWP_RENEW_MEMBERSHIP] TO ImpUsers
GO
