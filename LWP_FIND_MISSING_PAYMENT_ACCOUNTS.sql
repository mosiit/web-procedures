﻿USE [impresario]
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_FIND_MISSING_PAYMENT_ACCOUNTS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_FIND_MISSING_PAYMENT_ACCOUNTS]
GO

CREATE PROCEDURE [dbo].[LWP_FIND_MISSING_PAYMENT_ACCOUNTS]
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  SELECT DISTINCT
    o.customer_no,
    p.ccref_no as transId,
    p.pmt_amt,
    p.auth_no,
    p.act_id,
    pmt.act_type
  FROM [impresario].[dbo].[T_ORDER] as o
  LEFT JOIN [impresario].[dbo].[TX_CUST_KEYWORD] as attr
    ON attr.customer_no = o.customer_no
    AND attr.keyword_no = 417
  JOIN [impresario].[dbo].[T_TRANSACTION] as t
    ON t.order_no = o.order_no
  LEFT JOIN [impresario].[dbo].[T_LINEITEM] as li
    ON li.order_no = t.order_no
  LEFT JOIN [impresario].[dbo].[T_PERF] as perf
    ON perf.perf_no = li.perf_no
  JOIN [impresario].[dbo].[T_PAYMENT] as p
    ON p.transaction_no = t.transaction_no
    AND p.customer_no = o.customer_no
  JOIN [impresario].[dbo].[TX_CONST_CUST] as cons
    ON cons.customer_no = o.customer_no
  AND cons.constituency = 30
  JOIN [impresario].[dbo].[TR_PAYMENT_METHOD] as pmt
    ON p.pmt_method = pmt.id
  WHERE (MOS = 11 OR MOS = 20)
  AND attr.key_value IS NULL
  AND p.customer_no > 0
  AND p.ccref_no > 0
  AND p.pmt_amt > 60
  AND p.auth_no IS NOT NULL
  AND perf.facility_no = 25
  AND o.create_dt > DATEADD(year,-1,GETDATE())
END

GO
GRANT EXECUTE ON [dbo].[LWP_FIND_MISSING_PAYMENT_ACCOUNTS] TO ImpUsers
GO
