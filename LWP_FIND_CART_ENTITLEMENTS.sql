USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LWP_FIND_CART_ENTITLEMENTS]    Script Date: 9/22/2017 10:17:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LWP_FIND_CART_ENTITLEMENTS]

@sessionkey varchar(64) = NULL,
@debug char(1) = 'N'

AS

SET NOCOUNT ON

/************************************************************************
3/11/2015 Michael Reisman
Retrieve entitlement data for each cart item

SELECT * FROM T_WEB_CONTRIBUTION where order_no = 794
select * FROM T_WEB_ORDER where order_no = 794
select * FROM T_WEB_SUB_LINEITEM where order_no = 794

Sample execution:
EXEC dbo.LWP_FIND_CART_ENTITLEMENTS @sessionkey = 'XEJW8D2DC50Y956P10HUK74RUAAOBU116K00T7GTLQP9DWSIKHNENSTMOFQNJ043', @debug = 'N'

3/19/2015 MIR Uses entitlement key for when ent is other than membership-level reset
4/14/2015 MIR Return data even for non-entitlement combinations (left join, num_unused = zero not null)
4/24/2015 MIR Added numbers including those in the cart; 
				removed unnecessary columns; 
				Fixed logic to set the @price_type_to_use
4/28/2015 MIR Modified logic to get cart membership level.  We will not be getting it from T_WEB_CONTRIBUTION.memb_level_no so have to go through funds/camps
4/29/2015 MIR Added li_seq_no and memb_level_no to results set
5/4/2015 MIR Determines if cart contribution amount should be added to recog amount for an existing (non-cart) upgradeable membership for this customer.
5/12/2015 MIR when finding cart memberships, ignore where fund # is blank.
6/18/2015 MIR New column "is_entitlement_price" distinguishes non-entitlement prices vs. entitlement prices that aren't rated by member vs. exhausted entitlements
7/22/2016 MIR changed logic around upgrade/downgrade status = Ns vs BLANKS. Now will not grant because of eiether NO -OR- insufficient membership
************************************************************************/

Declare @errmsg varchar(200)
DECLARE @cart_memb_level_no int
DECLARE @cart_cont_amt money 
DECLARE @cart_fund_no int

DECLARE 
		@order_no int, 
		@order_dt datetime,
		@perf_no int,
		@price_type int,
		@zone_no int,
		@customer_no int

DECLARE @current_cart_ent int

/* all the sli detail vars */
DECLARE
	@sli_no int,
	@sli_detail_count int,
	@sli_detail_no int
		
/* this is the calculated repriced price of the item */
DECLARE @price money 		

/* set of output */		
DECLARE @mem_level_desc varchar(30),
		@entitlement_no int,
		@entitlement_key VARCHAR(255),
		@num_used int,
		@num_ent int,
		@num_unused int,
		@upgrade_downgrade char(1),
		@target_price_type int,
		@exceeded_price_type INT /*,
		@cascaded_ent_no INT,
		@cascaded_num_unused INT */
		
CREATE TABLE #cart_entitlement_count (entitlement_no int, entitlement_key VARCHAR(255), num_in_cart int)
	
If @sessionkey is null OR not exists (select * from [dbo].t_web_session_session where sessionkey = @sessionkey)
  Begin
	select @errmsg = 'Invalid Session ID'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	return -101
  End

SELECT
	sli.sli_no,
	sli.li_seq_no,
	o.order_dt,
	o.order_no,
	o.customer_no,
	li.perf_no,
	sli.price_type,
	sli.zone_no,
	entitlement_no = CAST(NULL AS INT),
	entitlement_key = CAST(NULL as varchar(255)),
	upgrade_downgrade = CAST(NULL AS VARCHAR(1)),
	target_price_type = CAST(NULL AS INT),
	exceeded_price_type = CAST(NULL AS INT),
	price_type_to_use = CAST(NULL AS INT),
	num_unused = CAST(NULL AS INT),
	message = CAST(NULL AS VARCHAR(4000)),
	is_entitlement_price = CAST(NULL as char(1))
INTO #cart
FROM T_WEB_ORDER o
	JOIN T_WEB_LINEITEM li on o.order_no = li.order_no
	JOIN T_WEB_SUB_LINEITEM sli ON sli.li_seq_no = li.li_seq_no
where o.sessionkey = @sessionkey
AND ISNULL(sli.sli_status,0) <>  4

SELECT @order_no = MIN(order_no),
		@order_dt = MIN(order_dt),
		@customer_no = MIN(customer_no)
FROM #cart

IF EXISTS (SELECT TOP 1 * FROM T_WEB_CONTRIBUTION WHERE order_no = @order_no AND NULLIF(memb_level_no,0) IS NOT NULL)
  BEGIN
	SELECT @cart_memb_level_no = MAX(memb_level_no) FROM dbo.T_WEB_CONTRIBUTION WHERE order_no = @order_no
  END
ELSE
  BEGIN
	/* Get the in-cart membership from data in T_WEB_CONTRIBUTION. There can be more than one contribution at one time in the cart, take highest amt */
	SET @cart_cont_amt = (SELECT MAX(contribution_amt) FROM T_WEB_CONTRIBUTION WHERE order_no = @order_no AND fund_no IS NOT NULL)
	SET @cart_fund_no = (SELECT fund_no FROM dbo.T_WEB_CONTRIBUTION WHERE order_no = @order_no AND contribution_amt = @cart_cont_amt AND fund_no IS NOT NULL)

	SET @cart_memb_level_no = (SELECT c.memb_level_no
	FROM TX_CAMP_FUND a
		JOIN T_CAMPAIGN b on a.campaign_no = b.campaign_no
		JOIN T_MEMB_LEVEL c on c.memb_org_no = b.memb_org_no 
		/* find current membership if there is one */
		LEFT JOIN
			(SELECT 
				ml.memb_org_no,
				ml.memb_level_no,				
				upgradeable_amt = CASE 
								/* if existing membership is not within renewal period */
								WHEN DATEDIFF(MONTH,CURRENT_TIMESTAMP,cm.expr_dt) > ml.renewal 
								/* specify the full amount paid as "upgradeable" */
								THEN cm.recog_amt
								ELSE 0 
								END
			FROM TX_CUST_MEMBERSHIP cm
				JOIN T_MEMB_LEVEL ml 
				ON (cm.memb_org_no = ml.memb_org_no and cm.memb_level = ml.memb_level and cm.cur_record = 'Y')
			WHERE cm.customer_no = @customer_no) cm
		ON cm.memb_org_no = c.memb_org_no
	where fund_no = @cart_fund_no 
	AND CURRENT_TIMESTAMP BETWEEN a.start_dt AND a.end_dt 
	/* the level is going to be represented by cart amt + current recog amount for upgradeable memberships */
	AND ISNULL(cm.upgradeable_amt,0) + @cart_cont_amt BETWEEN c.start_amt and c.end_amt 
	and b.memb_org_no is NOT NULL)

	IF @debug = 'Y' select cart_memb_level_no = @cart_memb_level_no
  END

/* Loop through the cart  */
SELECT @sli_no = (SELECT MIN(sli_no) FROM #cart)

WHILE @sli_no IS NOT NULL 
  BEGIN
	SELECT
		@perf_no  = perf_no,
		@price_type  = price_type /*,
		@zone_no = zone_no */
	FROM #cart
	WHERE sli_no = @sli_no

	EXEC dbo.LPP_GET_ENTITLEMENTS
		@memb_level_no = @cart_memb_level_no, 
		@customer_no = @customer_no, 
		@perf_no = @perf_no, 
		@price_type = @price_type,
		@return_results = 'N',
		@mem_level_desc_out = @mem_level_desc OUTPUT,
		@entitlement_no_out = @entitlement_no OUTPUT,
		@entitlement_key_out = @entitlement_key OUTPUT,
		@num_used_out = @num_used OUTPUT,
		@num_ent_out = @num_ent OUTPUT,
		@num_unused_out = @num_unused OUTPUT,
		@upgrade_downgrade_out = @upgrade_downgrade OUTPUT,
		@target_price_type_out = @target_price_type OUTPUT,
		@exceeded_price_type_out = @exceeded_price_type OUTPUT /*,
		@cascaded_ent_no_out = @cascaded_ent_no OUTPUT,
		@cascaded_num_unused_out = @cascaded_num_unused OUTPUT */

	/* store the entitlement num used/decremented */
	IF @entitlement_no IS NOT NULL
	AND NOT EXISTS (SELECT TOP 1 * FROM #cart_entitlement_count WHERE entitlement_no = @entitlement_no AND ISNULL(entitlement_key,'') = ISNULL(@entitlement_key,''))
		INSERT #cart_entitlement_count (entitlement_no, entitlement_key, num_in_cart) VALUES (@entitlement_no, @entitlement_key, 1)
	ELSE
		UPDATE #cart_entitlement_count SET num_in_cart = num_in_cart + 1
		WHERE entitlement_no = @entitlement_no
		AND ISNULL(entitlement_key,'') = ISNULL(@entitlement_key,'')
	
	SELECT
		@current_cart_ent = ISNULL((SELECT num_in_cart
	FROM #cart_entitlement_count 
	WHERE entitlement_no = @entitlement_no 
	AND ISNULL(entitlement_key,'') = ISNULL(@entitlement_key,'')),0)

	UPDATE #cart
	SET
		price_type_to_use = @price_type,
		target_price_type = @target_price_type,
		upgrade_downgrade = @upgrade_downgrade,
		exceeded_price_type = @exceeded_price_type,
		entitlement_no = @entitlement_no,
		entitlement_key = @entitlement_key,
		num_unused = ISNULL(@num_unused,0)
	WHERE sli_no = @sli_no
	
IF @upgrade_downgrade = 'N' OR (@upgrade_downgrade IS NULL AND @cart_memb_level_no > 0) /* 7/22/2016: Don't grant because no OR insufficient membership */
		UPDATE #cart
		SET
			price_type_to_use = target_price_type,
			is_entitlement_price = 'Y',
			message = 'You don''t have this entitlement.'
		WHERE sli_no = @sli_no
	/* if an entitlement exceeded */
	ELSE IF (@upgrade_downgrade = 'D' AND @current_cart_ent > @num_unused)
	--OR (@upgrade_downgrade = 'Z')
		UPDATE #cart
		SET
			price_type_to_use = target_price_type,
			is_entitlement_price = 'Y',
			message = 'You have exceeded your entitlements for at least one ticket in your cart.'
		WHERE sli_no = @sli_no
	/* if entitlement used */  
	ELSE IF (@upgrade_downgrade IN ('U','D') AND @current_cart_ent <= @num_unused)
		UPDATE #cart
		SET
			price_type_to_use = @price_type, /* 4/24/2015 */
is_entitlement_price = 'Y',
			message = 'Membership entitlement applied to your cart.'
		WHERE sli_no = @sli_no
	/* if entitlement exceeded EVEN just as the membership activated! greedy! */  
	ELSE IF @upgrade_downgrade = 'U' AND @current_cart_ent > @num_unused
		UPDATE #cart
		SET
			price_type_to_use = exceeded_price_type,
is_entitlement_price = 'Y',
			message = 'You have exceeded your entitlements for at least one ticket in your cart.'
		WHERE sli_no = @sli_no
	ELSE 
		UPDATE #cart
		SET
			is_entitlement_price = 'N',
			message = 'Not an entitlement.  Nothing to do.'
		WHERE sli_no = @sli_no
	SELECT @sli_no = (SELECT MIN(sli_no) FROM #cart WHERE sli_no > @sli_no)
  END 

/* Log to web table
INSERT dbo.LT_WEB_ENTITLEMENT_LOG (sessionkey, order_no) VALUES (@sessionkey, @order_no) */

DELETE #cart_entitlement_count WHERE entitlement_no = 0

SELECT
	c.sli_no,
	c.li_seq_no,
	c.order_dt,
	c.order_no,
	c.customer_no,
	c.perf_no,
	c.price_type,
	c.entitlement_no,
	e.memb_level_no,
	memb_level_desc = m.description,
	--c.zone_no,
	--c.upgrade_downgrade,
	--target_price_type,
	--exceeded_price_type,
	c.price_type_to_use,
	c.num_unused,
	num_used_in_cart = ISNULL(cec.num_in_cart,0),
	num_remain_after_cart = ISNULL(c.num_unused - cec.num_in_cart,0),
	c.is_entitlement_price,
	c.message,
	e.custom_1,
	e.custom_2,
	e.custom_3,
	e.custom_4,
	e.custom_5,
	e.custom_6,
	e.custom_7,
	e.custom_8,
	e.custom_9,
	e.custom_10
FROM #cart c
	LEFT JOIN #cart_entitlement_count cec ON (c.entitlement_no = cec.entitlement_no AND ISNULL(c.entitlement_key,'') = ISNULL(cec.entitlement_key,''))
	LEFT JOIN dbo.LTR_ENTITLEMENT e ON c.entitlement_no = e.entitlement_no
	LEFT JOIN dbo.T_MEMB_LEVEL m ON e.memb_level_no = m.memb_level_no
RETURN
Rollback_with_error:

--Rollback Tran
RAISERROR(@errmsg, 11, 2) WITH SETERROR
return -101

GO


