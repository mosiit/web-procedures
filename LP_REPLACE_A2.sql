USE [impresario]

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_REPLACE_A2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LP_REPLACE_A2]
GO

USE [impresario]

GO

CREATE PROCEDURE [dbo].[LP_REPLACE_A2]
  @session_key varchar(255) = null,
  @affiliate_constituent_no int

AS

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    DECLARE @errmsg varchar(255), @customer_no int = null

    IF @session_key is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @session_key)
        BEGIN
            SELECT @errmsg = 'Invalid Customer Session ID'
            RAISERROR(@errmsg, 11, 2) WITH SETERROR
            return -101
        END
    ELSE
        -- Assumes that the user is logged in as the Household
        SELECT @customer_no = customer_no
        FROM t_web_session_session
        WHERE sessionkey = @session_key

        IF @customer_no is null
            BEGIN
                SELECT @errmsg = 'User is not logged in'
                RAISERROR(@errmsg, 11, 2) WITH SETERROR
                return -101
            END
        ELSE
            UPDATE T_AFFILIATION SET name_ind = 0, primary_ind = 'N' WHERE group_customer_no = @customer_no AND name_ind = -2;
            UPDATE T_AFFILIATION SET name_ind = -2, primary_ind = 'Y' WHERE group_customer_no = @customer_no AND individual_customer_no = @affiliate_constituent_no;


GO
 
GRANT EXECUTE ON [dbo].[LP_REPLACE_A2] TO ImpUsers
GO
