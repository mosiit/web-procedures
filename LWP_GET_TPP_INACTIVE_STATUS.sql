USE [impresario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_GET_TPP_INACTIVE_STATUS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_GET_TPP_INACTIVE_STATUS]
GO

CREATE PROCEDURE [dbo].[LWP_GET_TPP_INACTIVE_STATUS]
	@sessionkey VARCHAR(255),
	@TPPToken VARCHAR(255),
	@email VARCHAR(255)
AS
BEGIN

  DECLARE @errmsg varchar(255);

	If @sessionkey is null OR not exists (select * from t_web_session_session where sessionkey = @sessionkey)
	  Begin
		select @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End
	Else
		SELECT inactive FROM [dbo].[LTR_WEB_TPP_TOKEN]
		WHERE token = @TPPToken AND email = @email

END

GO

GRANT EXECUTE ON [dbo].[LWP_GET_TPP_INACTIVE_STATUS] TO ImpUsers
GO
