USE [impresario];
GO

/****** Object:  StoredProcedure [dbo].[LWP_CHANGE_PAYMENT_METHOD]    Script Date: 4/12/2016 10:39:34 AM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


ALTER PROCEDURE [dbo].[LWP_CHANGE_PAYMENT_METHOD]
    @sessionkey VARCHAR(64),
    @payment_no INT,
    @pmt_method INT,
    @last_four VARCHAR(4),
    @act_id INT
    WITH EXECUTE AS 'dbo'
AS
BEGIN
	
    DECLARE @customer_no INT,
        @id_key INT,
        @errmsg VARCHAR(255),
        @mapped_pmt_method INT;

	-- validate sessionkey parameter
    IF @sessionkey IS NULL
        OR NOT EXISTS ( SELECT  *
                        FROM    t_web_session_Session
                        WHERE   SessionKey = @sessionkey )
        BEGIN
            SELECT  @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey;
            RAISERROR(@errmsg, 11, 2) WITH SETERROR;
            RETURN -101;
        END;
    ELSE
        SELECT  @customer_no = customer_no
        FROM    t_web_session_Session
        WHERE   SessionKey = @sessionkey;

	-- Validate the payment methods on the order are correct

    IF NOT EXISTS ( SELECT  *
                    FROM    T_WEB_PAYMENT
                    WHERE   sessionkey = @sessionkey
                            AND payment_no = @payment_no
                            AND pmt_method = @pmt_method )
        BEGIN
            RAISERROR('Could not find matching payment in T_WEB_PAYMENT', 11, 2) WITH SETERROR;
            RETURN -101;	
        END;

	-- Validate that a mapping has been set for the payment method being passed in

    IF NOT EXISTS ( SELECT  *
                    FROM    LTR_WEB_PMT_METHOD_MAPPING
                    WHERE   non_cc_payment_method = @pmt_method )
        BEGIN
            SELECT  @errmsg = 'Could not find payment method '
                    + CAST(@pmt_method AS VARCHAR)
                    + ' in LTR_WEB_PMT_METHOD_MAPPING';
            RAISERROR(@errmsg, 11, 2) WITH SETERROR;
            RETURN -101;	
        END;
    ELSE
        SELECT  @mapped_pmt_method = cc_payment_method
        FROM    LTR_WEB_PMT_METHOD_MAPPING
        WHERE   non_cc_payment_method = @pmt_method;


	-- Flip the payment method

    UPDATE  T_PAYMENT
    SET     pmt_method = @mapped_pmt_method,
            act_id = @act_id,
            account_no = @last_four
    WHERE   payment_no = @payment_no
            AND customer_no = @customer_no;

    IF @@ROWCOUNT > 0
        BEGIN 
            SELECT  'Y' AS success;
        END; 
    ELSE
        SELECT  'N' AS success;
		
END;

GRANT EXECUTE ON [dbo].[LWP_CHANGE_PAYMENT_METHOD] TO ImpUsers;

GO


