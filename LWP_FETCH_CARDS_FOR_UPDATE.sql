USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_FETCH_CARDS_FOR_UPDATE]    Script Date: 9/12/2017 11:50:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LWP_FETCH_CARDS_FOR_UPDATE]
AS
BEGIN

  SELECT u.customer_no, u.payment_account_id, p.token, p.merchant_id
    FROM [impresario].[dbo].[LTR_CARD_UPDATES] AS u
    JOIN [impresario].[dbo].[T_ACCOUNT_DATA] AS p 
      ON u.customer_no = p.customer_no AND u.payment_account_id = p.id
    WHERE DATEDIFF(day, GETDATE(), CAST(u.process_dt AS DATE)) <= 1
    
END

GO
GRANT EXECUTE ON [dbo].[LWP_FETCH_CARDS_FOR_UPDATE] TO ImpUsers
GO
