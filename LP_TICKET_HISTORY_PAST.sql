USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_TICKET_HISTORY_PAST]
	@sessionkey VARCHAR(64) = null
AS

DECLARE @errmsg VARCHAR(200)
DECLARE @customer_no INT
DECLARE @now DATETIME

SET @now = GetDate()

IF @sessionkey is not null and @sessionkey > '' and not exists (SELECT * FROM [dbo].t_web_session_session WHERE sessionkey = @sessionkey)
  BEGIN
	SELECT @errmsg = 'Invalid Session ID'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	RETURN -101
  End

IF @sessionkey is not null
	SELECT	@customer_no = customer_no
	FROM	[dbo].t_web_session_session
	WHERE	sessionkey = @sessionkey

SELECT li.*, ord.order_dt, ord.delivery, webord.sessionkey
INTO #li
FROM [dbo].T_LINEITEM li (NOLOCK)
INNER JOIN [dbo].T_Order ord (NOLOCK) ON li.order_no = ord.order_no
INNER JOIN [dbo].T_Web_Order webord (NOLOCK) ON li.order_no = webord.order_no
WHERE ord.customer_no = @customer_no

SELECT sli.*
INTO #sli
FROM [dbo].T_SUB_LINEITEM sli  (NOLOCK)
INNER JOIN #li li ON sli.li_seq_no = li.li_seq_no

-- Lineitems
SELECT
	li.li_seq_no,
	li.li_no,
	li.order_no,
	orderTab.MOS,
	li.order_dt,
	li.sessionkey,
	li.delivery as 'shipping_method_id',
	shipMethod.description as 'shipping_method_desc',
	perf.prod_season_no,
	perf.perf_type as 'prod_type',
	perftype.description as 'prod_type_desc',
	li.perf_no,
	perf.perf_code,
	perf.perf_dt,
	inv.description as 'perf_desc',
	perf.facility_no,
	li.priority,
	li.primary_ind,
	fac.description as 'facility_desc',
	fac.th_no as 'theater_no',
	theater.description as 'theater_desc',
	perf.zmap_no,
	req.notes

INTO #line_item
FROM #li li
LEFT OUTER JOIN [dbo].TR_ORDER_SHIP_METHOD shipMethod (NOLOCK) ON li.delivery = shipMethod.id
LEFT OUTER JOIN [dbo].T_PERF perf (NOLOCK) ON li.perf_no = perf.perf_no
LEFT OUTER JOIN [dbo].T_ORDER orderTab (NOLOCK) ON li.order_no = orderTab.order_no
INNER JOIN T_INVENTORY inv (NOLOCK) ON perf.perf_no = inv.inv_no
INNER JOIN [dbo].TR_PERF_TYPE perftype (NOLOCK) ON perf.perf_type = perftype.id
INNER JOIN [dbo].T_FACILITY fac (NOLOCK) ON perf.facility_no = fac.facil_no
INNER JOIN [dbo].TR_THEATER theater (NOLOCK) ON fac.th_no = theater.id
LEFT OUTER JOIN [dbo].T_SPECIAL_REQ req (NOLOCK) ON li.li_seq_no = req.li_seq_no
WHERE perf.perf_dt < GETDATE()
ORDER BY li.li_seq_no

-- sub lineitem
SELECT
	sli.li_seq_no,
	sli.sli_no,
	sli.due_amt ,
	sli.paid_amt ,
	sli.price_type ,
	perftype.description as 'price_type_desc',
	sli.seat_no,
	seat.seat_row,
	seat.seat_num,
	sli.zone_no,
	zone.description as 'zone_desc',
	seat.section,
	sect.description as 'section_desc',
	sect.short_desc as 'section_short_desc',
	sect.print_desc as 'section_print_desc',
	sect.additional_text as 'section_add_text',
	sect.additional_text2 as 'section_add_text2',
	sli.ret_parent_sli_no,
	sli.comp_code,
	sli.sli_status,
	status.description as 'status_desc'

INTO #subline_item
FROM #sli sli
INNER JOIN [dbo].T_PERF perf (NOLOCK) ON sli.perf_no = perf.perf_no and sli.perf_no > 0
LEFT OUTER JOIN [dbo].TR_PRICE_TYPE perftype (NOLOCK) ON sli.price_type = perftype.id
LEFT OUTER JOIN [dbo].T_SEAT seat (NOLOCK) ON sli.seat_no = seat.seat_no and sli.seat_no > 0
INNER JOIN [dbo].T_ZONE zone (NOLOCK) ON sli.zone_no = zone.zone_no and sli.zone_no = zone.zone_no
LEFT OUTER JOIN [dbo].TR_SECTION sect (NOLOCK) ON seat.section = sect.id
LEFT OUTER JOIN [dbo].TR_SLI_STATUS status (NOLOCK) ON sli.sli_status = status.id
WHERE perf.perf_dt < GETDATE() AND ISNULL(sli.perf_no, 0) > 0
ORDER BY sli.li_seq_no, sli.sli_no

/****** Output data ******/
SELECT * FROM #line_item
SELECT * FROM #subline_item

GO
