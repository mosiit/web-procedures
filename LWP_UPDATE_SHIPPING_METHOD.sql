USE [impresario]

GO
/****   (2015-09-17)    ****/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_UPDATE_SHIPPING_METHOD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_UPDATE_SHIPPING_METHOD]
GO

USE [impresario]

GO

CREATE PROCEDURE [dbo].[LWP_UPDATE_SHIPPING_METHOD]
    @SessionKey varchar(255),
    @order_no int,
    @shipping_method int
AS
    UPDATE T_ORDER SET delivery = @shipping_method WHERE order_no = @order_no;
GO