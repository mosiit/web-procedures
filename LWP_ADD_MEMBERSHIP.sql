USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LWP_ADD_MEMBERSHIP]    Script Date: 12/29/2016 9:52:11 AM ******/
DROP PROCEDURE [dbo].[LWP_ADD_MEMBERSHIP]
GO

/****** Object:  StoredProcedure [dbo].[LWP_ADD_MEMBERSHIP]    Script Date: 12/29/2016 9:52:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[LWP_ADD_MEMBERSHIP]
       @sessionkey VARCHAR(64),
       @campaignNo INT,
       @membershipOrgLevel INT,
       @membershipLevel VARCHAR(3)
AS 
       BEGIN
	
             DECLARE @customer_no INT,
                     @id_key INT,
                     @errmsg VARCHAR(255),
                     @cust_memb_no INT

	-- validate sessionkey parameter
             IF @sessionkey IS NULL
                OR NOT EXISTS ( SELECT  *
                                FROM    t_web_session_session
                                WHERE   sessionkey = @sessionkey ) 
                BEGIN
                      SELECT    @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
                      RAISERROR(@errmsg, 11, 2) WITH SETERROR
                      RETURN -101
                END
             ELSE 
                SELECT  @customer_no = customer_no
                FROM    t_web_session_session
                WHERE   sessionkey = @sessionkey

             EXEC @cust_memb_no = [dbo].ap_get_nextid_function 'CM'

             INSERT [dbo].tx_cust_membership
                    (cust_memb_no,
                     parent_no,
                     customer_no,
                     campaign_no,
                     memb_org_no,
                     memb_level,
                     memb_amt,
                     ben_provider,
                     init_dt,
                     expr_dt,
                     current_status,
                     NRR_status,
                     memb_trend,
                     declined_ind,
                     AVC_amt,
                     orig_expiry_dt,
                     orig_memb_level,
                     inception_dt,
                     mir_lock,
                     cur_record,
                     recog_amt,
                     category_trend,
                     first_issue_id,
                     final_issue_id,
                     num_copies
                    )
             VALUES (@cust_memb_no,
                     0,
                     @customer_no,
                     @campaignNo,
                     @membershipOrgLevel,
                     @membershipLevel,
                     0,
                     0,		-- replaces "0", so we can create a gift membership FP1525.
                     GETDATE(),
                     DATEADD(yyyy, 1, GETDATE()),
                     2,				-- New memberships are always active.
                     'NE',	-- new
                     4,
                     'N',
                     0,
                     DATEADD(yyyy, 1, GETDATE()),
                     @membershipLevel,
                     GETDATE(),		-- replaces above CWR 4/19/2010 FP1560.
                     0,
                     'Y',
                     0,
                     4,
                     NULL,
                     NULL,
                     NULL
                    )
       END

GO

GRANT EXECUTE ON [dbo].[LWP_ADD_MEMBERSHIP] TO impusers
GO