USE [impresario]

GO

ALTER PROCEDURE [dbo].[LP_MEMBER_LOGIN_EX]
  @loginType INT,
  @membershipId INT,
  @zipcode varchar(5) = NULL,
  @zipCodeAuth tinyint = 0
AS 
  SELECT c.customer_no AS constituent_no,
    CASE WHEN l.login_type IS NULL THEN 'false'
      ELSE 'true'
    END AS has_login
  FROM [impresario].[dbo].[TX_CUST_MEMBERSHIP] AS c
  LEFT JOIN [impresario].[dbo].[T_CUST_LOGIN] AS l ON l.customer_no = c.customer_no
    AND l.login_type = @loginType
  WHERE c.customer_no = @membershipId
  AND c.cust_memb_no <> ''
  AND c.current_status = 2
  AND (@zipCodeAuth = 0 OR c.customer_no = (
        SELECT a.customer_no FROM [impresario].[dbo].[T_ADDRESS] AS a
        WHERE a.customer_no = c.customer_no
        AND a.primary_ind = 'Y'
        AND SUBSTRING(a.postal_code, 1, 5) = @zipcode
      ));
GO
