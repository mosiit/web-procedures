USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_GET_CUST_ENTITLEMENTS_USED]    Script Date: 9/22/2017 10:18:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LWP_GET_CUST_ENTITLEMENTS_USED]

@customer_no int,
@calendar_date datetime = NULL,
@reset_type char(1) = 'A' /* D=Daily; M=Membership; A (or null)=All */

/* Sample execution:
Exec dbo.LWP_GET_CUST_ENTITLEMENTS_USED @customer_no = 70039, @calendar_date = '6/3/2015', @reset_type = 'M'

6/22/2015 MIR inactive status should not be granted entitlements - only (2,3)
*/

AS

SET @calendar_date = ISNULL(@calendar_date, 
					(SELECT CAST(CONVERT(varchar(10),CURRENT_TIMESTAMP,101) as datetime)))

SELECT 
	ent_hist_id = MIN(coe.id_key),
	order_no  = coe.order_no,
	order_dt = o.order_dt,
	date_of_visit = f.perf_dt,
	e.entitlement_no,
	entitlement_type = kw.description,
	item_desc = ps_inv.description + ISNULL(' ' + f.perf_code,'') + CASE WHEN coe.order_no = 0 THEN ' (Scan)' ELSE '' END,
	price_type = COALESCE(pt.description,ptg.description),
	number_used = SUM(coe.num_used)
FROM dbo.LTX_CUST_ORDER_ENTITLEMENT coe
	JOIN dbo.LTR_ENTITLEMENT e on coe.entitlement_no = e.entitlement_no
	JOIN dbo.TR_PRICE_TYPE_GROUP ptg ON e.ent_price_type_group = ptg.id
	LEFT JOIN T_SUB_LINEITEM sli
		JOIN T_ORDER o ON sli.order_no = o.order_no
		JOIN TR_PRICE_TYPE pt ON pt.id = sli.price_type
	ON coe.sli_no = sli.sli_no
	JOIN T_PERF f ON f.perf_no = coe.perf_no
	JOIN T_INVENTORY ps_inv ON f.prod_season_no = ps_inv.inv_no
	JOIN dbo.TR_TKW kw on kw.id = e.ent_tkw_id
	JOIN dbo.TX_CUST_MEMBERSHIP cm ON (cm.cust_memb_no = coe.cust_memb_no /* AND cm.cur_record = 'Y' */)
	JOIN dbo.LV_MEMB_LEVELS ml ON ml.memb_level_no = e.memb_level_no
WHERE coe.customer_no = @customer_no
/* MFA */
AND (
	(cm.current_status in (2,3) and @calendar_date BETWEEN cm.init_dt AND cm.expr_dt)
	OR (cm.current_status = 3 AND @calendar_date BETWEEN DATEADD(MONTH,0-ml.renewal,cm.init_dt) AND cm.expr_dt) --pending 
	)
/* end MFA */
AND (
	(reset_type = 'D' AND coe.entitlement_date = @calendar_date)
	OR reset_type = 'M'
	)
AND (
	ISNULL(@reset_type,'A') = 'A'
	OR e.reset_type = @reset_type
	)	
GROUP BY coe.order_no, o.order_dt, f.perf_dt, e.entitlement_no, kw.description, ps_inv.description, f.perf_code, pt.description, ptg.description
