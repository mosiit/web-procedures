USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_ADD_CUST_FROM_CARD_TOKEN]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_ADD_CUST_FROM_CARD_TOKEN]
GO

CREATE PROCEDURE [dbo].[LP_ADD_CUST_FROM_CARD_TOKEN]
        @SessionKey VARCHAR(255) = null,
        @customerNo INT = null,
    	@lastDigits VARCHAR(4) = null,
    	@expiryMonth VARCHAR(2) = null,
	    @expiryYear VARCHAR(2) = null
AS BEGIN

   IF NOT EXISTS (SELECT * FROM [dbo].[LTR_CUSTOMER_CARD_TOKEN]
                  WHERE customer_no = @customerNo AND last_digits = @lastDigits AND expiry_month = @expiryMonth AND expiry_year = @expiryYear)
   BEGIN
       INSERT INTO [dbo].[LTR_CUSTOMER_CARD_TOKEN] (customer_no, last_digits, expiry_month, expiry_year)
        VALUES (@customerNo, @lastDigits, @expiryMonth, @expiryYear)
   END

END
GO

GRANT EXECUTE ON [dbo].[LP_ADD_CUST_FROM_CARD_TOKEN] TO impusers
GO

IF not exists (SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_ADD_CUST_FROM_CARD_TOKEN') BEGIN

    DECLARE @nextID int
    SELECT @nextID = (max([id]) + 1) FROM [dbo].[TR_LOCAL_PROCEDURE]
    IF @nextID is null SELECT @nextID = 1

    INSERT INTO [dbo].[TR_LOCAL_PROCEDURE] ([id], [description], [procedure_name]) VALUES (@NextID, 'Add Customer Card Token','LP_ADD_CUST_FROM_CARD_TOKEN')

END
GO

SELECT * FROM [dbo].[TR_LOCAL_PROCEDURE] WHERE [procedure_name] = 'LP_ADD_CUST_FROM_CARD_TOKEN'

