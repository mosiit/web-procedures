USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LWP_MEMBERSHIP_GIFT_LOOKUP]
    @sessionkey VARCHAR(255),
    @membership_no INT,
    @email VARCHAR(255),
    @zip VARCHAR(255),
    @lastName VARCHAR(255)
AS BEGIN

    DECLARE @errmsg VARCHAR(255)

    IF @sessionkey IS NULL OR NOT EXISTS (SELECT * FROM t_web_session_session WHERE sessionkey = @sessionkey) BEGIN

      SELECT @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
      RAISERROR(@errmsg, 11, 2) WITH SETERROR
      RETURN -101

    END ELSE BEGIN

      SELECT customer.customer_no
	  INTO #membershipsViaNumber
        FROM T_CUSTOMER customer
        JOIN T_EADDRESS eaddress
        ON eaddress.primary_ind = 'Y' AND customer.customer_no = eaddress.customer_no
        WHERE eaddress.address = @email AND customer.customer_no = @membership_no
        
      IF (SELECT COUNT(*) FROM  #membershipsViaNumber) = 1 BEGIN

        SELECT * FROM #membershipsViaNumber
        RETURN

      END

      SELECT customer.customer_no
	  INTO #membershipsViaAddress
        FROM T_CUSTOMER customer
        JOIN T_ADDRESS address
        ON address.primary_ind = 'Y' AND customer.customer_no = address.customer_no
        WHERE address.postal_code LIKE @zip + '%' AND customer.lname = @lastName AND
		(
		    customer.customer_no = @membership_no OR
			customer.customer_no IN (SELECT individual_customer_no FROM T_AFFILIATION WHERE group_customer_no = @membership_no)
		)

      IF (SELECT COUNT(*) FROM  #membershipsViaAddress) = 1 BEGIN

        SELECT * FROM #membershipsViaAddress
        RETURN

      END

      RETURN

 END
END
GO


