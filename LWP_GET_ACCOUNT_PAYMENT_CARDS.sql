USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LWP_GET_ACCOUNT_PAYMENT_CARDS]    Script Date: 02/01/2016 09:11:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LWP_GET_ACCOUNT_PAYMENT_CARDS]    Script Date: 05/30/2014 07:54:15 ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[LWP_GET_ACCOUNT_PAYMENT_CARDS]')
                    AND type IN (N'P', N'PC') ) 
   DROP PROCEDURE [dbo].[LWP_GET_ACCOUNT_PAYMENT_CARDS]
GO

CREATE PROCEDURE [dbo].[LWP_GET_ACCOUNT_PAYMENT_CARDS]
       @sessionkey VARCHAR(64)
       WITH EXECUTE AS 'dbo'
AS 
       BEGIN
	
             DECLARE @customer_no INT,
                     @id_key INT,
                     @errmsg VARCHAR(255)

	-- validate sessionkey parameter
             IF @sessionkey IS NULL
                OR NOT EXISTS ( SELECT  *
                                FROM    t_web_session_session
                                WHERE   sessionkey = @sessionkey ) 
                BEGIN
                      SELECT    @errmsg = 'Invalid Customer Session ID :: ' + @sessionkey
                      RAISERROR(@errmsg, 11, 2) WITH SETERROR
                      RETURN -101
                END
             ELSE 
                SELECT  @customer_no = customer_no
                FROM    t_web_session_session
                WHERE   sessionkey = @sessionkey

             SELECT account_data.id AS act_id,
                    account_type.description,
                    token,
                    card_expiry_dt,
                    act_no_four,
                    act_name,
                    account_data.create_dt
             FROM   T_ACCOUNT_DATA AS account_data
             INNER JOIN [impresario].[dbo].[TR_ACCOUNT_TYPE] AS account_type ON account_data.act_type = account_type.id
             WHERE  customer_no = @customer_no
                    AND account_data.inactive = 'N'
       END

       GRANT EXECUTE ON [dbo].[LWP_GET_ACCOUNT_PAYMENT_CARDS] TO impusers
GO