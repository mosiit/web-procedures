USE [impresario]

GO
/****   (2015-09-17)    ****/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA]
GO

USE [impresario]

GO

CREATE PROCEDURE [dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA]
  @session_key varchar(255) = null,
  @order_no int,
  @bookingData text,
  @configData text,
  @selectedPerformanceData text,
  @todaysEventData text

AS

    DECLARE @errmsg varchar(255), @customer_no int = null

    IF @session_key is null OR not exists (SELECT * FROM t_web_session_session WHERE sessionkey = @session_key)
        BEGIN
            SELECT @errmsg = 'Invalid Customer Session ID'
            RAISERROR(@errmsg, 11, 2) WITH SETERROR
            return -101
        END
    ELSE

        IF NOT EXISTS (SELECT * FROM LWT_Field_Trip_Order_Data WHERE order_no = @order_no)
          BEGIN
            INSERT INTO LWT_Field_Trip_Order_Data
        (
            order_no,
            bookingData,
            configData,
            selectedPerformanceData,
            todaysEventData
        ) VALUES (
            @order_no,
            @bookingData,
            @configData,
            @selectedPerformanceData,
            @todaysEventData
        );
          END
        ELSE
          UPDATE LWT_Field_Trip_Order_Data SET bookingData = @bookingData, configData = @configData, selectedPerformanceData = @selectedPerformanceData, todaysEventData = @todaysEventData
          WHERE order_no = @order_no
GO

GRANT ALTER ON [dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA] TO [MOS.ORG\lthornton]
GO
GRANT CONTROL ON [dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA] TO [MOS.ORG\lthornton]
GO
GRANT EXECUTE ON [dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA] TO [MOS.ORG\lthornton]
GO

GRANT EXECUTE ON [dbo].[LWP_ADD_FIELD_TRIP_ORDER_DATA] TO ImpUsers
GO