USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_MEMBER_LOGIN]    Script Date: 10/20/2016 3:47:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_MEMBER_LOGIN]
       @loginType INT,
       @membershipId INT
AS 
       SELECT   c.customer_no AS constituent_no,
                CASE WHEN l.login_type IS NULL THEN 'false'
                     ELSE 'true'
                END AS has_login
       FROM     [impresario].[dbo].[TX_CUST_MEMBERSHIP] AS c
       LEFT JOIN [impresario].[dbo].[T_CUST_LOGIN] AS l ON l.customer_no = c.customer_no
                                                           AND l.login_type = @loginType
       WHERE    c.customer_no = @membershipId
	   AND		cust_memb_no <> '';
                --AND cust_memb_no != '';


GO


