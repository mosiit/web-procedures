﻿USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD_ONE_STEP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD_ONE_STEP]
GO

CREATE PROCEDURE [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD_ONE_STEP]
	@sessionkey VARCHAR(64),
	@token VARCHAR(64),
	@act_type INT,
	@expiry_month VARCHAR(2),
	@expiry_year  VARCHAR(4),
	@act_no_four  VARCHAR(4),
	@act_no_six  VARCHAR(6),
	@act_name VARCHAR(128),
	@merch_id VARCHAR(128),
	@inactive VARCHAR(1),
	@customer_no INT
  with execute as 'dbo'
AS
BEGIN

  SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  Declare @expire_time DATETIME;
  Declare @now DATETIME;

  set @expire_time = CAST(CAST(@expiry_year AS varchar) + '-' + CAST(@expiry_month AS varchar) + '-1' AS DATETIME);
  set @now = GETDATE();

  OPEN SYMMETRIC KEY TessituraSecureKey
  DECRYPTION BY CERTIFICATE TessCert

  IF EXISTS (SELECT * FROM [dbo].[T_ACCOUNT_DATA] WHERE customer_no = @customer_no AND act_type = @act_type
    AND card_expiry_dt = @expire_time AND act_no_four = @act_no_four)
  BEGIN
    SELECT id FROM [dbo].[T_ACCOUNT_DATA] WHERE customer_no = @customer_no AND act_type = @act_type
    AND card_expiry_dt = @expire_time AND act_no_four = @act_no_four
  END
ELSE
  BEGIN
  DECLARE @act_no VARCHAR = CONVERT(VARCHAR, @act_no_six) + '--' + CONVERT(VARCHAR, @act_no_four);
  EXEC [dbo].[AP_STORE_ACCOUNT_DATA]
		@customer_no = @customer_no,
		@act_no = @act_no,
		@act_type = @act_type,
		@act_name = @act_name,
		@card_expiry_dt = @expire_time,
		@cc_issue_no = NULL,
		@cc_start_dt = @now,
		@inactive = @inactive,
		@insert_update_flag = 'I',
		@return_data = 'N',
		@bank_identifier_code = NULL,
		@mandate_type = NULL,
		@mandate_number = NULL,
		@token = @token,
		@payment_method_group_id = 2,
		@merchant_id = @merch_id,
		@user_id = 'IMP User',
		@create_loc = 'Web'
  END

	CLOSE SYMMETRIC KEY TessituraSecureKey

END

GO
GRANT EXECUTE ON [dbo].[LWP_ADD_ACCOUNT_PAYMENT_CARD_ONE_STEP] TO ImpUsers
GO
